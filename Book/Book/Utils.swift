//
//  Utils.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class Utils {
  //Validate Email Form
  func emailValidation(email: String?) -> Bool {
    let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
    return emailPredicate.evaluate(with: email)
  }
  
  func allertView(msg: String) -> UIView {
    let customView = UIView()
    
    let imgTitle = UIImage(named:"Alert_Img")
    let imgViewTitle = UIImageView()
    imgViewTitle.image = imgTitle
    
    let label = UILabel()
    label.text = msg
    label.numberOfLines = 0
    label.font = UIFont.systemFont(ofSize: 12.0)
    label.textAlignment = .center
    
    customView.addSubview(imgViewTitle)
    customView.addSubview(label)
    
    imgViewTitle.snp.makeConstraints{ make in
      make.top.equalTo(customView.snp.top).offset(10)
      make.centerX.equalTo(customView.snp.centerX)
      make.width.height.equalTo(40)
    }
    
    label.snp.makeConstraints{ make in
      make.top.equalTo(imgViewTitle.snp.bottom).offset(10)
      make.left.equalTo(customView.snp.left).offset(10)
      make.right.equalTo(customView.snp.right).offset(-10)
      make.bottom.equalTo(customView.snp.bottom).offset(-3)
    }
    
    customView.layer.cornerRadius = 10.0
    customView.clipsToBounds = true
    customView.backgroundColor = UIColor.lightGray
    
    return customView
  }
  
  //Custom iOS Allert
  func allertAction(message: String) -> UIAlertController {
    let alertController = UIAlertController(title: "", message: nil, preferredStyle: .alert)
    alertController.view.subviews.first!.isHidden = true
    let customView = allertView(msg: message)
    alertController.view.addSubview(customView)
    customView.snp.makeConstraints{ make in
      make.left.equalTo(alertController.view).offset(20)
      make.right.equalTo(alertController.view).offset(-20)
      make.centerY.equalTo(alertController.view)
      make.height.equalTo(alertController.view.frame.height/5)
    }
    
    return alertController
  }
  
  //Native iOS Allert
  func nativeAllertAction(message: String) -> UIAlertController {
    let alertController = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
    let actionOk = UIAlertAction(title: "Ok",
                                 style: .default,
                                 handler: nil)
    alertController.addAction(actionOk)
    
    return alertController
  }
}
