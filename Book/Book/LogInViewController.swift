//
//  LogInViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit
import Foundation
import SnapKit
import CountryPicker
import TwicketSegmentedControl

class LogInViewController: UIViewController, UITextFieldDelegate {
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  override var shouldAutorotate: Bool {
    return false
  }
  
  override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
    return UIInterfaceOrientation.portrait
  }
  
  
  
  var backgroundImage: UIImageView!
  var iconAppImage: UIImageView!
  var appNameLabel: UILabel!
  var infoLabel: UILabel!
  var emailTextField: TextField!
  var passwordTextField: TextField!
  var phoneTextField: TextField!
  var signLabel: UILabel!
  var loginChoiceSC: TwicketSegmentedControl!
  var forgetPasswordButton: UIButton!
  var logInButton: UIButton!
  var signInButton: UIButton!
  var skipButton: UIButton!
  var shopButton: UIButton!

  var pickerView: CountryPicker!
  let locale = Locale.current
  var code: String!
  
  var isFromTaleVC = false
  
  let utils = Utils()
  
  let attributes = [
    NSForegroundColorAttributeName: UIColor.white,
    NSFontAttributeName : UIFont(name: "PTSans-Regular", size: 14.0)
  ]
  
  override func viewDidLoad() {
    super.viewDidLoad()
     UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
    
//    for family: String in UIFont.familyNames {
//      print("\(family)")
//      for names: String in UIFont.fontNames(forFamilyName: family){
//        print("== \(names)")
//      }
//    }
    navigationController?.isNavigationBarHidden = true
    setupViews()
    setupConstraints()
  }

  func setupViews() {
    backgroundImage = UIImageView()
    backgroundImage.image = appBackgroundImage
    
    code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
    
    iconAppImage = UIImageView()
    iconAppImage.contentMode = .scaleAspectFit
    iconAppImage.image = app_Icon
    iconAppImage.clipsToBounds = true
    
    appNameLabel = UILabel()
    appNameLabel.textAlignment = .center
    appNameLabel.adjustsFontSizeToFitWidth = true
    appNameLabel.textColor = UIColor.white
    appNameLabel.font = UIFont(name: "PTSans-Bold", size: 30.0)
    appNameLabel.text = appName
    
    infoLabel = UILabel()
    infoLabel.textAlignment = .center
    infoLabel.adjustsFontSizeToFitWidth = true
    infoLabel.textColor = UIColor.white
    infoLabel.font = UIFont(name: "PTSans-Regular", size: 17.0)
    infoLabel.text = "Login with"
    
    loginChoiceSC = TwicketSegmentedControl()
    loginChoiceSC.setSegmentItems(["Email", "Phone Number"])
    loginChoiceSC.move(to: 0)
    loginChoiceSC.delegate = self
    loginChoiceSC.font = UIFont(name: "PTSans-Regular", size: 13.0)!
    
    loginChoiceSC.defaultTextColor = .white
    loginChoiceSC.highlightTextColor = .white
    loginChoiceSC.segmentsBackgroundColor = buttonColorThree
    loginChoiceSC.sliderBackgroundColor = mainButtonColor
    loginChoiceSC.backgroundColor = .clear
    
    emailTextField = TextField()
    emailTextField.keyboardType = UIKeyboardType.alphabet
    emailTextField.attributedPlaceholder = NSAttributedString(string: "Email Address", attributes:attributes)
    emailTextField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
    emailTextField.returnKeyType = .next
    emailTextField.textColor = UIColor.white
    emailTextField.delegate = self
    emailTextField.backgroundColor = buttonColorThree
    
    phoneTextField = TextField()
    phoneTextField.keyboardType = UIKeyboardType.decimalPad
    phoneTextField.attributedPlaceholder = NSAttributedString(string: "Phone Number", attributes:attributes)
    phoneTextField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
    phoneTextField.returnKeyType = .next
    phoneTextField.textColor = UIColor.white
    phoneTextField.delegate = self
    phoneTextField.backgroundColor = buttonColorThree
    phoneTextField.isHidden = true
    
    pickerView = CountryPicker()
    pickerView.countryPickerDelegate = self
    pickerView.showPhoneNumbers = true
    pickerView.setCountry(code!)
    pickerView.isHidden = true
    pickerView.backgroundColor = .clear
    
    passwordTextField = TextField()
    passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: attributes)
    passwordTextField.returnKeyType = .done
    passwordTextField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
    passwordTextField.textColor = UIColor.white
    passwordTextField.backgroundColor = buttonColorThree
    passwordTextField.delegate = self
    passwordTextField.isSecureTextEntry = true
    
    forgetPasswordButton = UIButton()
    forgetPasswordButton.setTitle("Forgot Password?", for: .normal)
    forgetPasswordButton.titleLabel?.font = UIFont(name: "PTSans-Regular", size: 17.0) 
    forgetPasswordButton.setTitleColor(UIColor.white, for: .normal)
    forgetPasswordButton.titleLabel?.textAlignment = .center
    forgetPasswordButton.backgroundColor = .clear
    forgetPasswordButton.addTarget(self, action: #selector(forgotPassword(_:)), for: .touchUpInside)
    
    logInButton = UIButton()
    logInButton.titleLabel?.font = UIFont(name: "PTSans-Bold", size: 11.0)
    logInButton.setTitle("L O G I N", for: .normal)
    logInButton.setTitleColor(UIColor.white, for: .normal)
    logInButton.titleLabel?.textAlignment = .center
    logInButton.layer.cornerRadius = 20
    logInButton.backgroundColor = mainButtonColor
    logInButton.addTarget(self, action: #selector(logIn(_:)), for: .touchUpInside)
    
    signLabel = UILabel()
    signLabel.textAlignment = .center
    signLabel.adjustsFontSizeToFitWidth = true
    signLabel.textColor = UIColor.white
    signLabel.font = UIFont(name: "PTSans-Regular", size: 17.0)
    signLabel.text = "Don't have an account?"
    
    signInButton = UIButton()
    signInButton.titleLabel?.font =  UIFont(name: "PTSans-Regular", size: 17.0)
    signInButton.setTitle("Sign In", for: .normal)
    signInButton.setTitleColor(UIColor(colorLiteralRed: 244/255, green: 81/255, blue: 185/255, alpha: 1.0), for: .normal)
    signInButton.titleLabel?.textAlignment = .left
    signInButton.backgroundColor = .clear
    signInButton.addTarget(self, action: #selector(signUp(_:)), for: .touchUpInside)
    
    skipButton = UIButton()
    skipButton.titleLabel?.font = UIFont(name: "PTSans-Regular", size: 14.0)
    skipButton.setTitle("Skip", for: .normal)
    skipButton.setTitleColor(UIColor.white, for: .normal)
    skipButton.titleLabel?.textAlignment = .center
    skipButton.layer.cornerRadius = 5
    skipButton.backgroundColor = buttonColorTwo
    skipButton.addTarget(self, action: #selector(skip(_:)), for: .touchUpInside)
    
    shopButton = UIButton()
    shopButton.setBackgroundImage(UIImage(named: "shop-icon"), for: .normal)
    shopButton.addTarget(self, action: #selector(shop(_:)), for: .touchUpInside)
    
    let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
    toolBar.barStyle = UIBarStyle.default
    toolBar.items = [
      UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonAction))]
    toolBar.sizeToFit()    
    phoneTextField.inputAccessoryView = toolBar
    
    self.view.addSubview(backgroundImage)
    self.view.addSubview(iconAppImage)
    self.view.addSubview(appNameLabel)
    self.view.addSubview(infoLabel)
    self.view.addSubview(loginChoiceSC)
    self.view.addSubview(emailTextField)
    self.view.addSubview(pickerView)
    self.view.addSubview(phoneTextField)
    self.view.addSubview(passwordTextField)
    self.view.addSubview(forgetPasswordButton)
    self.view.addSubview(logInButton)
    self.view.addSubview(signLabel)
    self.view.addSubview(signInButton)
    self.view.addSubview(skipButton)
    self.view.addSubview(shopButton)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self.view)
    }
    
    iconAppImage.snp.makeConstraints{ make in
      make.top.equalTo(self.view.frame.height/13)
      make.centerX.equalTo(self.view)
      make.width.height.equalTo(self.view.frame.width/3)
    }
    
    appNameLabel.snp.makeConstraints{ make in
      make.top.equalTo(iconAppImage.snp.bottom)
      make.centerX.equalTo(self.view)
    }
    
    infoLabel.snp.makeConstraints{ make in
      make.top.equalTo(appNameLabel.snp.bottom).offset(5)
      make.centerX.equalTo(self.view)
    }
    
    loginChoiceSC.snp.makeConstraints{ make in
      make.top.equalTo(infoLabel.snp.bottom).offset(20)
      make.centerX.equalTo(self.view)
      make.width.equalTo(self.view.frame.width/1.8)
      make.height.equalTo(40)
    }
    
    pickerView.snp.makeConstraints{ make in
      make.top.equalTo(loginChoiceSC.snp.bottom).offset(15)
      make.left.equalTo(self.view).offset(self.view.frame.width/12)
      make.width.equalTo(self.view.frame.width/2.5)
      make.height.equalTo(70)
    }
    
    emailTextField.snp.makeConstraints{ make in
      make.top.equalTo(loginChoiceSC.snp.bottom).offset(30)
      make.left.equalTo(self.view).offset(self.view.frame.width/8)
      make.right.equalTo(self.view).offset(-self.view.frame.width/8)
      make.height.equalTo(45)
    }
    
    phoneTextField.snp.makeConstraints{ make in
      make.top.equalTo(loginChoiceSC.snp.bottom).offset(30)
      make.left.equalTo(pickerView.snp.right).offset(10)
      make.right.equalTo(self.view).offset(-self.view.frame.width/8)
      make.height.equalTo(45)
    }
    
    passwordTextField.snp.makeConstraints{ make in
      make.top.equalTo(emailTextField.snp.bottom).offset(10)
      make.left.equalTo(self.view).offset(self.view.frame.width/8)
      make.right.equalTo(self.view).offset(-self.view.frame.width/8)
      make.height.equalTo(45)
    }
    
    forgetPasswordButton.snp.makeConstraints{ make in
      make.top.equalTo(passwordTextField.snp.bottom).offset(10)
      make.centerX.equalTo(self.view)
      make.width.equalTo(self.view.frame.width/2)
    }
    
    logInButton.snp.makeConstraints{ make in
      make.top.equalTo(forgetPasswordButton.snp.bottom).offset(10)
      make.centerX.equalTo(self.view)
      make.width.equalTo(self.view.frame.width/1.8)
      make.height.equalTo(45)
    }
    
    signLabel.snp.makeConstraints{ make in
      make.top.equalTo(logInButton.snp.bottom).offset(10)
      make.centerX.equalTo(logInButton.snp.centerX).offset(-30)
    }
    
    signInButton.snp.makeConstraints{ make in
      make.left.equalTo(signLabel.snp.right).offset(3)
      make.centerY.equalTo(signLabel.snp.centerY)
      make.height.equalTo(30)
    }
    
    skipButton.snp.makeConstraints{ make in
      make.bottom.equalTo(self.view).offset(-20)
      make.centerX.equalTo(self.view)
      make.width.equalTo(self.view.frame.width/4)
      make.height.equalTo(45)
    }
    
    shopButton.snp.makeConstraints{ make in
      make.right.equalTo(skipButton.snp.left).offset(-40)
      make.centerY.equalTo(skipButton.snp.centerY)
      make.width.equalTo(40)
      make.height.equalTo(40)
    }
  }
  
  func doneButtonAction()
  {
    self.phoneTextField.resignFirstResponder()
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    if textField == emailTextField {
      passwordTextField.becomeFirstResponder()
    } else if textField == passwordTextField {
      passwordTextField.resignFirstResponder()
    }
    return true
  }
  
  func forgotPassword(_ sender: UIButton) {
    self.navigationController?.pushViewController(ForgotPasswordVC(), animated: true)
  }
  
  func logIn(_ sender: UIButton){
    if loginChoiceSC.selectedSegmentIndex == 0 {
      if (emailTextField.text! == "" || passwordTextField.text! == "") {
        let alert = utils.allertAction(message: "Email Address and Password fields are required")
        self.present(alert, animated: true, completion: nil)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
          self.dismiss(animated: true, completion: nil)
        }
      } else if utils.emailValidation(email: emailTextField.text) == false  {
        self.present(utils.allertAction(message: "Invalid email"), animated: true, completion: nil)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
          self.dismiss(animated: true, completion: nil)
        }
      } else if passwordTextField.text! == "" {
        self.present(utils.allertAction(message: "Password is equired"), animated: true, completion: nil)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
          self.dismiss(animated: true, completion: nil)
        }
      } else  {
        //Register User
        //Push to home vc
      }
    } else {
      if (emailTextField.text! == "") {
        self.present(utils.nativeAllertAction(message: "Please enter your phone number"), animated: true, completion: nil)
      } else if passwordTextField.text! == "" {
        self.present(utils.nativeAllertAction(message: "Password is equired"), animated: true, completion: nil)
      } else  {
        //Register User
        //Push to home vc
      }
    }
  }
  
  func signUp(_ sender: UIButton) {
    self.navigationController?.pushViewController(SignUpViewController(), animated: true)
  }
  
  func shop(_ sender: UIButton) {
    self.navigationController?.pushViewController(ChooseLanguagesViewController(), animated: true)
  }
  
  func skip(_ sender: UIButton) {
    if isFromTaleVC {
      self.dismiss(animated: true, completion: nil)
    } else {
      self.navigationController?.pushViewController(HomeViewController(), animated: true)
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

extension LogInViewController: TwicketSegmentedControlDelegate {
  func didSelect(_ segmentIndex: Int) {
    switch segmentIndex {
    case 0:
      self.view.endEditing(true)
      pickerView.isHidden = true
      phoneTextField.isHidden = true
      emailTextField.isHidden = false
    case 1:
      self.view.endEditing(true)
      emailTextField.isHidden = true
      pickerView.isHidden = false
      phoneTextField.isHidden = false
    default:
      break
    }

    print("Selected index: \(segmentIndex)")
  }
}

extension LogInViewController: CountryPickerDelegate {
  
  func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
    code = phoneCode
  }
}

class TextField: UITextField {
  let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5);
  
  override func textRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }
  
  override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }
  
  override func editingRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }
}

extension UITextField{
  
  func addDoneButtonToKeyboard(myAction:Selector?){
    let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 300, height: 40))
    doneToolbar.barStyle = UIBarStyle.default
    
    let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
    let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: myAction)
    
    var items = [UIBarButtonItem]()
    items.append(flexSpace)
    items.append(done)
    
    doneToolbar.items = items
    doneToolbar.sizeToFit()
    
    self.inputAccessoryView = doneToolbar
  }
}
