//
//  HowItWorksViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit

class HowItWorksViewController: UIViewController {
  var backgroundImage: UIImageView!
  var iconAppImage: UIImageView!
  var scrollTitle: UILabel!
  var scrollView: UIScrollView!
  var pageControl: UIPageControl!
  
  var pageImages:[UIImage] = [UIImage(named: "how1")!, UIImage(named: "How2")!, UIImage(named: "How3")!]
  var beforePage = 1000
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    setupViews()
    setupConstraints()
  }
  
  func setupViews() {
    backgroundImage = UIImageView()
    backgroundImage.image = UIImage(named: "Background")
    
    scrollTitle = UILabel()
    scrollTitle.textAlignment = .center
    scrollTitle.adjustsFontSizeToFitWidth = true
    scrollTitle.textColor = UIColor.white
    scrollTitle.font = UIFont(name: "PTSans-Regular", size: 20.0)
    scrollTitle.text = "Touch Guide"
    
    iconAppImage = UIImageView()
    iconAppImage.contentMode = .scaleAspectFit
    iconAppImage.image = app_Icon
    iconAppImage.clipsToBounds = true
    
    pageControl = UIPageControl(frame: CGRect(x:0,y: 0, width:200, height:50))
    pageControl.numberOfPages = pageImages.count
    pageControl.currentPage = 0
    pageControl.pageIndicatorTintColor = UIColor.lightGray
    pageControl.currentPageIndicatorTintColor = UIColor.white
    
    scrollView = UIScrollView(frame: CGRect(x:0, y:0, width:self.view.frame.size.width,height: self.view.frame.size.height.divided(by: 2)))
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
    scrollView.delegate = self
    scrollView.isPagingEnabled = true
    scrollView.showsHorizontalScrollIndicator = false
    scrollView.contentSize = CGSize(width:self.scrollView.frame.size.width * 3, height: self.scrollView.frame.size.height)
    for index in 0..<pageImages.count {
      frame.origin.x = CGFloat(index) * scrollView.frame.size.width
      frame.size = self.scrollView.frame.size
      
      let imgViewRecipe = UIImageView(frame: frame)
      imgViewRecipe.contentMode = .scaleAspectFit
      imgViewRecipe.image = pageImages[index]
      
      let view = TouchControlView(frame: frame)
      self.scrollView.addSubview(view)
    }
    pageControl.addTarget(self, action: #selector(self.changePage(sender:)), for: UIControlEvents.valueChanged)
    
    self.view.addSubview(backgroundImage)
    self.view.addSubview(iconAppImage)
    self.view.addSubview(scrollTitle)
    self.view.addSubview(scrollView)
    self.view.addSubview(pageControl)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self.view)
    }
    
    iconAppImage.snp.makeConstraints{ make in
      make.top.equalTo(self.view).offset(50)
      make.centerX.equalTo(self.view)
      make.width.height.equalTo(self.view.frame.width/5)
    }
    
    scrollTitle.snp.makeConstraints{ make in
      make.top.equalTo(iconAppImage.snp.bottom).offset(20)
      make.centerX.equalTo(self.view)
      make.left.equalTo(self.view).offset(30)
      make.right.equalTo(self.view).offset(-30)
    }
    
    scrollView.snp.makeConstraints{ make in
      make.top.equalTo(scrollTitle.snp.bottom)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
      make.height.equalTo(self.view).dividedBy(2)
    }
    
    pageControl.snp.makeConstraints{ make in
      make.top.equalTo(scrollView.snp.bottom)
      make.centerX.equalTo(scrollView.snp.centerX)
    }
  }
  
  func moveToNextPage() {
    let pageWidth:CGFloat = self.scrollView.frame.width
    let maxWidth:CGFloat = pageWidth * 3
    let contentOffset:CGFloat = self.scrollView.contentOffset.x
    var pageNumber: CGFloat
    var slideToX = contentOffset + pageWidth
    
    pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width) + 1
    if  contentOffset + pageWidth == maxWidth {
      slideToX = 0
      pageNumber = 0
    }
    pageControl.currentPage = Int(pageNumber)
    self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height.divided(by: 2)), animated: true)
  }
  
  func changePage(sender: AnyObject) -> () {
    let x = CGFloat(pageControl.currentPage) * scrollView.frame.size.width
    scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

extension HowItWorksViewController: UIScrollViewDelegate {
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
    pageControl.currentPage = Int(pageNumber)
    
    let translation = scrollView.panGestureRecognizer.translation(in: scrollView.superview!)
    if translation.x < 0 {
      if beforePage == Int(pageNumber){
        if beforePage == (pageImages.count - 1) {
          let transition = CATransition()
          transition.duration = 0.5
          transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
          transition.type = kCATransitionFade
          self.navigationController?.view.layer.add(transition, forKey: nil)
          _ = self.navigationController?.popViewController(animated: false)
        }
      }
    }
    
    beforePage = Int(pageNumber)
  }
}

