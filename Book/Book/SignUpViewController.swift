//
//  SignUpViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit
import Foundation
import SnapKit
import CountryPicker
import TwicketSegmentedControl

class SignUpViewController: UIViewController, UITextFieldDelegate {
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return .portrait
  }
  
  var backgroundImage: UIImageView!
  var iconAppImage: UIImageView!
  var appNameLabel: UILabel!
  var infoLabel: UILabel!
  var emailTextField : TextField!
  var passwordTextField  : TextField!
  var phoneTextField: TextField!
  var loginChoiceSC: TwicketSegmentedControl!
  var nextButton: UIButton!
  var backButton: UIButton!
  
  var pickerView: CountryPicker!
  let locale = Locale.current
  var code: String!
  
  let utils = Utils()
  
  let attributes = [
    NSForegroundColorAttributeName: UIColor.white,
    NSFontAttributeName : UIFont(name: "PTSans-Regular", size: 14.0)
  ]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    setupViews()
    setupConstraints()
  }
  
  
  func setupViews() {
    backgroundImage = UIImageView()
    backgroundImage.image = appBackgroundImage
    
    code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
    
    iconAppImage = UIImageView()
    iconAppImage.contentMode = .scaleAspectFit
    iconAppImage.image = app_Icon
    iconAppImage.clipsToBounds = true
    
    appNameLabel = UILabel()
    appNameLabel.textAlignment = .center
    appNameLabel.adjustsFontSizeToFitWidth = true
    appNameLabel.textColor = UIColor.white
    appNameLabel.font = UIFont(name: "PTSans-Bold", size: 30.0)
    appNameLabel.text = appName
    
    infoLabel = UILabel()
    infoLabel.textAlignment = .center
    infoLabel.adjustsFontSizeToFitWidth = true
    infoLabel.textColor = UIColor.white
    infoLabel.font = UIFont(name: "PTSans-Regular", size: 17.0)
    infoLabel.text = "Register with your"
    
    loginChoiceSC = TwicketSegmentedControl()
    loginChoiceSC.setSegmentItems(["Email", "Phone Number"])
    loginChoiceSC.move(to: 0)
    loginChoiceSC.delegate = self
    loginChoiceSC.font = UIFont(name: "PTSans-Regular", size: 13.0)!
    
    loginChoiceSC.defaultTextColor = .white
    loginChoiceSC.highlightTextColor = .white
    loginChoiceSC.segmentsBackgroundColor = buttonColorThree
    loginChoiceSC.sliderBackgroundColor = mainButtonColor
    loginChoiceSC.backgroundColor = .clear
    
    emailTextField = TextField()
    emailTextField.attributedPlaceholder = NSAttributedString(string: "Email Address", attributes:attributes)
    emailTextField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
    emailTextField.returnKeyType = .next
    emailTextField.delegate = self
    emailTextField.backgroundColor = buttonColorThree
    emailTextField.textColor = UIColor.white
    
    pickerView = CountryPicker()
    pickerView.countryPickerDelegate = self
    pickerView.showPhoneNumbers = true
    pickerView.setCountry(code!)
    pickerView.isHidden = true
    pickerView.backgroundColor = .clear
    
    phoneTextField = TextField()
    phoneTextField.keyboardType = UIKeyboardType.decimalPad
    phoneTextField.attributedPlaceholder = NSAttributedString(string: "Phone Number", attributes:attributes)
    phoneTextField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
    phoneTextField.returnKeyType = .next
    phoneTextField.textColor = UIColor.white
    phoneTextField.delegate = self
    phoneTextField.backgroundColor = buttonColorThree
    phoneTextField.isHidden = true
    
    passwordTextField = TextField()
    passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: attributes)
    passwordTextField.returnKeyType = .done
    passwordTextField.contentVerticalAlignment = UIControlContentVerticalAlignment.center
    passwordTextField.textColor = UIColor.white
    passwordTextField.backgroundColor = buttonColorThree
    passwordTextField.delegate = self
    passwordTextField.isSecureTextEntry = true
    
    nextButton = UIButton()
    nextButton.setTitle("Next", for: .normal)
    nextButton.setTitleColor(UIColor.white, for: .normal)
    nextButton.titleLabel?.textAlignment = .center
    nextButton.layer.cornerRadius = 15
    nextButton.backgroundColor = mainButtonColor
    nextButton.addTarget(self, action: #selector(next(_:)), for: .touchUpInside)
    
    backButton = UIButton()
    backButton.setBackgroundImage(UIImage(named: "btn_back.png"), for: .normal)
    backButton.backgroundColor = UIColor.clear
    backButton.addTarget(self, action: #selector(back(_:)), for: .touchUpInside)
    
    let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
    toolBar.barStyle = UIBarStyle.default
    toolBar.items = [
      UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneButtonAction))]
    toolBar.sizeToFit()
    phoneTextField.inputAccessoryView = toolBar
    
    self.view.addSubview(backgroundImage)
    self.view.addSubview(iconAppImage)
    self.view.addSubview(appNameLabel)
    self.view.addSubview(infoLabel)
    self.view.addSubview(loginChoiceSC)
    self.view.addSubview(emailTextField)
    self.view.addSubview(pickerView)
    self.view.addSubview(phoneTextField)
    self.view.addSubview(passwordTextField)
    self.view.addSubview(nextButton)
    self.view.addSubview(backButton)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self.view)
    }
    
    backButton.snp.makeConstraints{ make in
      make.top.equalTo(self.view).offset(20)
      make.left.equalTo(self.view).offset(10)
      make.height.width.equalTo(self.view.frame.width/13)
    }
    
    iconAppImage.snp.makeConstraints{ make in
      make.top.equalTo(self.view.frame.height/13)
      make.centerX.equalTo(self.view)
      make.width.height.equalTo(self.view.frame.width/3)
    }
    
    appNameLabel.snp.makeConstraints{ make in
      make.top.equalTo(iconAppImage.snp.bottom)
      make.centerX.equalTo(self.view)
    }
    
    infoLabel.snp.makeConstraints{ make in
      make.top.equalTo(appNameLabel.snp.bottom).offset(5)
      make.centerX.equalTo(self.view)
    }
    
    loginChoiceSC.snp.makeConstraints{ make in
      make.top.equalTo(infoLabel.snp.bottom).offset(10)
      make.centerX.equalTo(self.view)
      make.width.equalTo(self.view.frame.width/1.8)
      make.height.equalTo(45)
    }
    
    pickerView.snp.makeConstraints{ make in
      make.top.equalTo(loginChoiceSC.snp.bottom).offset(15)
      make.left.equalTo(self.view).offset(self.view.frame.width/12)
      make.width.equalTo(self.view.frame.width/2.5)
      make.height.equalTo(70)
    }
    
    phoneTextField.snp.makeConstraints{ make in
      make.top.equalTo(loginChoiceSC.snp.bottom).offset(30)
      make.left.equalTo(pickerView.snp.right).offset(10)
      make.right.equalTo(self.view).offset(-self.view.frame.width/8)
      make.height.equalTo(45)
    }
    
    emailTextField.snp.makeConstraints{ make in
      make.top.equalTo(loginChoiceSC.snp.bottom).offset(30)
      make.left.equalTo(self.view).offset(self.view.frame.width/8)
      make.right.equalTo(self.view).offset(-self.view.frame.width/8)
      make.height.equalTo(45)
    }
    
    passwordTextField.snp.makeConstraints{ make in
      make.top.equalTo(emailTextField.snp.bottom).offset(10)
      make.left.equalTo(self.view).offset(self.view.frame.width/8)
      make.right.equalTo(self.view).offset(-self.view.frame.width/8)
      make.height.equalTo(45)
    }
    
    emailTextField.snp.makeConstraints{ make in
      make.top.equalTo(loginChoiceSC.snp.bottom).offset(30)
      make.left.equalTo(self.view).offset(self.view.frame.width/8)
      make.right.equalTo(self.view).offset(-self.view.frame.width/8)
      make.height.equalTo(45)
    }
    
    nextButton.snp.makeConstraints{ make in
      make.bottom.equalTo(self.view).offset(-30)
      make.left.equalTo(self.view).offset(self.view.frame.width/8)
      make.right.equalTo(self.view).offset(-self.view.frame.width/8)
      make.height.equalTo(40)
    }
  }

  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    if textField == emailTextField {
      passwordTextField.becomeFirstResponder()
    } else if textField == passwordTextField {
      passwordTextField.resignFirstResponder()
    }
    return true
  }
  
  func next(_ sender: UIButton){
    if loginChoiceSC.selectedSegmentIndex == 0 {
      if (emailTextField.text! == "" || passwordTextField.text! == "") {
        let alert = utils.allertAction(message: "Email Address and Password fields are required")
        self.present(alert, animated: true, completion: nil)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
          self.dismiss(animated: true, completion: nil)
        }
      } else if utils.emailValidation(email: emailTextField.text) == false  {
        self.present(utils.allertAction(message: "Invalid email"), animated: true, completion: nil)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
          self.dismiss(animated: true, completion: nil)
        }
      } else if passwordTextField.text! == "" {
        self.present(utils.allertAction(message: "Password is equired"), animated: true, completion: nil)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
          self.dismiss(animated: true, completion: nil)
        }
      } else  {
        self.present(utils.nativeAllertAction(message: "Sucessful signup"), animated: true, completion: nil)
        //Register User
        //Push to home vc
      }
    } else {
      if (emailTextField.text! == "") {
        self.present(utils.nativeAllertAction(message: "Please enter your phone number"), animated: true, completion: nil)
      } else if passwordTextField.text! == "" {
        self.present(utils.nativeAllertAction(message: "Password is equired"), animated: true, completion: nil)
      } else  {
        self.present(utils.nativeAllertAction(message: "Sucessful signup"), animated: true, completion: nil)
        //Register User
        //Push to home vc
      }
    }
  }
  
  func doneButtonAction()
  {
    self.phoneTextField.resignFirstResponder()
  }
  
  func back(_ sender: UIButton){
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

extension SignUpViewController: TwicketSegmentedControlDelegate {
  func didSelect(_ segmentIndex: Int) {
    switch segmentIndex {
    case 0:
      self.view.endEditing(true)
      pickerView.isHidden = true
      phoneTextField.isHidden = true
      emailTextField.isHidden = false
    case 1:
      self.view.endEditing(true)
      emailTextField.isHidden = true
      pickerView.isHidden = false
      phoneTextField.isHidden = false
    default:
      break
    }
  }
}

extension SignUpViewController: CountryPickerDelegate {
  func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
    code = phoneCode
  }
}


