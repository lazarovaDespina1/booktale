//
//  TouchControlView.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import Foundation
import UIKit

class TouchControlView: UIView {
  var titleOneLabel : UILabel!
  var titleOneImageView : UIImageView!
  var titleTwoLabel : UILabel!
  var titleTwoImageView : UIImageView!
  var titleThreeLabel : UILabel!
  var titleThreeImageView : UIImageView!
  var titleFourLabel : UILabel!
  var titleFourImageView : UIImageView!
  
  var titles = ["Two finger swipe \n Back to cover",
                "Two finger tap: \n Reset audio",
                "One finger swipe: \n Change page",
                "Tap on object/words: \n See what happen"]
  var images = ["support", "works", "privacy", "terms"]
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    self.backgroundColor = UIColor.clear
    
    titleOneLabel = createLabel(title: titles[0])
    titleTwoLabel = createLabel(title: titles[1])
    titleThreeLabel = createLabel(title: titles[2])
    titleFourLabel = createLabel(title: titles[3])
    
    titleOneImageView = createImageView(image: images[0])
    titleTwoImageView = createImageView(image: images[1])
    titleThreeImageView = createImageView(image: images[2])
    titleFourImageView = createImageView(image: images[3])
    
    self.addSubview(titleOneLabel)
    self.addSubview(titleOneImageView)
    self.addSubview(titleTwoLabel)
    self.addSubview(titleTwoImageView)
    self.addSubview(titleThreeLabel)
    self.addSubview(titleThreeImageView)
    self.addSubview(titleFourLabel)
    self.addSubview(titleFourImageView)
  }
  
  func setupConstraints() {
    titleOneLabel.snp.makeConstraints{ make in
      make.top.equalTo(self).offset(20)
      make.left.equalTo(self).offset(20)
    }
    
    titleOneImageView.snp.makeConstraints{ make in
      make.top.equalTo(titleOneLabel.snp.bottom).offset(10)
      make.centerX.equalTo(titleOneLabel.snp.centerX)
      make.width.height.equalTo(80)
    }
    
    titleTwoLabel.snp.makeConstraints{ make in
      make.top.equalTo(self).offset(20)
      make.right.equalTo(self).offset(-20)
    }
    
    titleTwoImageView.snp.makeConstraints{ make in
      make.top.equalTo(titleTwoLabel.snp.bottom).offset(10)
      make.centerX.equalTo(titleTwoLabel.snp.centerX)
      make.width.height.equalTo(80)
    }
    
    
    titleThreeLabel.snp.makeConstraints{ make in
      make.top.equalTo(titleOneImageView.snp.bottom).offset(20)
      make.left.equalTo(self).offset(20)
    }
    
    titleThreeImageView.snp.makeConstraints{ make in
      make.top.equalTo(titleThreeLabel.snp.bottom).offset(10)
      make.centerX.equalTo(titleOneLabel.snp.centerX)
      make.width.height.equalTo(80)
    }
    
    titleFourLabel.snp.makeConstraints{ make in
      make.top.equalTo(titleTwoImageView.snp.bottom).offset(20)
      make.right.equalTo(self).offset(-20)
    }
    
    titleFourImageView.snp.makeConstraints{ make in
      make.top.equalTo(titleFourLabel.snp.bottom).offset(10)
      make.centerX.equalTo(titleFourLabel.snp.centerX)
      make.width.height.equalTo(80)
    }
  }
  
  func createLabel(title: String) -> UILabel{
    let label = UILabel()
    label.textAlignment = .center
    label.adjustsFontSizeToFitWidth = true
    label.textColor = UIColor.white
    label.font = UIFont(name: "PTSans-Regular", size: 18.0)
    label.text = title
    label.numberOfLines = 0
    return label
  }
  
  func createImageView(image: String) -> UIImageView {
    let imageV = UIImageView()
    imageV.contentMode = .scaleAspectFit
    imageV.image = UIImage(named: image)
    return imageV
  }
  
}
