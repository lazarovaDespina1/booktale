//
//  LanguagesTableViewCell.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit

class LanguagesTableViewCell: UITableViewCell {
  var languageLabel: UILabel!
  var leftImageView: UIImageView!
  var freeTrialButton: UIButton!
  var priceButton: UIButton!
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    contentView.backgroundColor = UIColor.clear
    
    languageLabel = UILabel()
    languageLabel.textColor = UIColor.white
    languageLabel.textAlignment = .left
    languageLabel.font = UIFont(name: "PTSans-Bold", size: 17.0)
    
    leftImageView = UIImageView()
    leftImageView.contentMode = .scaleAspectFit
    leftImageView.image = UIImage(named: "living-room-books-group")
    
    freeTrialButton = UIButton()
    freeTrialButton.setTitle("Free trial", for: .normal)
    freeTrialButton.setTitleColor(UIColor.white, for: .normal)
    freeTrialButton.titleLabel?.textAlignment = .center
    freeTrialButton.backgroundColor = .black
    
    priceButton = UIButton()
    priceButton.setTitleColor(UIColor.white, for: .normal)
    priceButton.titleLabel?.textAlignment = .center
    priceButton.layer.cornerRadius = 15
    priceButton.backgroundColor = UIColor(colorLiteralRed: 255 / 255, green: 179 / 255, blue: 13 / 255, alpha: 1.0)
    
    contentView.addSubview(leftImageView)
    contentView.addSubview(languageLabel)
    contentView.addSubview(freeTrialButton)
  }
  
  func setupConstraints() {
    leftImageView.snp.makeConstraints{ make in
      make.centerY.equalTo(contentView)
      make.left.equalTo(contentView)
      make.width.height.equalTo(contentView.snp.width).dividedBy(7)
    }
    
    languageLabel.snp.makeConstraints{ make in
      make.centerY.equalTo(contentView)
      make.left.equalTo(leftImageView.snp.right).offset(10)
      make.right.equalTo(contentView)
    }
    
    freeTrialButton.snp.makeConstraints{ make in
      make.top.equalTo(contentView.snp.top)
      make.bottom.equalTo(contentView.snp.bottom)
      make.left.equalTo(languageLabel.snp.right)
      make.right.equalTo(contentView.snp.right)
    }
  }
  
  func setupCell(language: String, price: String) {
    self.languageLabel.text = language
    self.priceButton.setTitle(price, for: .normal)
  }
}


