//
//  ChooseLanguagesViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit
import Foundation
import SnapKit

class ChooseLanguagesViewController: UIViewController {
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return .portrait
  }
  
  var backgroundImage: UIImageView!
  var backButton: UIButton!
  var titleLabel: UILabel!
  var tableView: UITableView!
  var restoreButton: UIButton!
  
  var isComeFromeHome = false
  var languages: [String] = [String]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    setupViews()
    setupConstraints()
  }
  
  func setupViews() {
    backgroundImage = UIImageView()
    backgroundImage.image = appBackgroundImage
    
    languages = ["English","Norwegian", "Chinese", "Swedish", "Danish"]
    
    backButton = UIButton()
    backButton.setBackgroundImage(UIImage(named: "btn_back.png"), for: .normal)
    backButton.backgroundColor = UIColor.clear
    backButton.addTarget(self, action: #selector(back(_:)), for: .touchUpInside)
    
    titleLabel = UILabel()
    titleLabel.textAlignment = .left
    titleLabel.adjustsFontSizeToFitWidth = true
    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont(name: "PTSans-Regular", size: 17.0)
    titleLabel.text = "Choose your languages"
    
    tableView = UITableView()
    tableView.backgroundColor = UIColor.clear
    tableView.register(LanguagesTableViewCell.self, forCellReuseIdentifier: "cell")
    tableView.dataSource = self
    tableView.delegate = self
    
    restoreButton = UIButton()
    restoreButton.titleLabel?.font =  UIFont(name: "PTSans-Regular", size: 25.0)
    restoreButton.setTitle("Restore", for: .normal)
    restoreButton.setTitleColor(UIColor.white, for: .normal)
    restoreButton.titleLabel?.textAlignment = .center
    restoreButton.layer.cornerRadius = 15
    restoreButton.backgroundColor = buttonColorTwo
    
    self.view.addSubview(backgroundImage)
    self.view.addSubview(restoreButton)
    self.view.addSubview(backButton)
    self.view.addSubview(titleLabel)
    self.view.addSubview(tableView)
    self.view.addSubview(restoreButton)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self.view)
    }
    
    backButton.snp.makeConstraints{ make in
      make.top.equalTo(self.view).offset(20)
      make.left.equalTo(self.view).offset(30)
      make.height.width.equalTo(self.view.frame.width/13)
    }
    
    titleLabel.snp.makeConstraints{ make in
      make.centerY.equalTo(backButton.snp.centerY)
      make.left.equalTo(backButton.snp.right).offset(10)
    }
    
    tableView.snp.makeConstraints{ make in
      make.left.equalTo(self.view).offset(10)
      make.right.equalTo(self.view).offset(-10)
      make.top.equalTo(titleLabel.snp.bottom).offset(20)
      make.height.equalTo(self.view.frame.height/2)
    }
    
    restoreButton.snp.makeConstraints{ make in
      make.left.equalTo(self.view).offset(20)
      make.right.equalTo(self.view).offset(-20)
      make.top.equalTo(tableView.snp.bottom).offset(20)
      make.height.equalTo(55)
    }
  }
  
  func back(_ sender: UIButton){
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

extension ChooseLanguagesViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LanguagesTableViewCell
    cell.setupCell(language: languages[indexPath.row], price: "$4.99")
    cell.backgroundColor = .clear
    cell.selectionStyle = .none
    
    let freeTrialButton = UIButton()
    freeTrialButton.titleLabel?.font =  UIFont(name: "PTSans-Regular", size: 14.0)
    freeTrialButton.setTitle("Free trial", for: .normal)
    freeTrialButton.setTitleColor(UIColor.white, for: .normal)
    freeTrialButton.titleLabel?.textAlignment = .center
    freeTrialButton.layer.cornerRadius = 15
    freeTrialButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
    freeTrialButton.backgroundColor = mainButtonColor
    
    freeTrialButton.frame = CGRect(x: (cell.contentView.frame.width/5) * 2, y: (cell.contentView.center.y)-((cell.contentView.frame.height/1.5)/2), width: cell.contentView.frame.width/4, height: cell.contentView.frame.height/1.5)
    cell.contentView.addSubview(freeTrialButton)
    
    let priceButton = UIButton()
    freeTrialButton.titleLabel?.font =  UIFont(name: "PTSans-Regular", size: 14.0)
    priceButton.setTitle("$4.99", for: .normal)
    priceButton.setTitleColor(UIColor.white, for: .normal)
    priceButton.titleLabel?.textAlignment = .center
    priceButton.layer.cornerRadius = 15
    priceButton.backgroundColor = buttonColorTwo
    priceButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
    
    priceButton.frame = CGRect(x: (cell.contentView.frame.width/3) * 2, y: (cell.contentView.center.y)-((cell.contentView.frame.height/1.5)/2), width: cell.contentView.frame.width/3, height: cell.contentView.frame.height/1.5)
    cell.contentView.addSubview(priceButton)
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 64
  }
}
