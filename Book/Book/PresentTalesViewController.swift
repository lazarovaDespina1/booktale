//
//  PresentTalesViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//


import UIKit
import Foundation
import SnapKit

class PresentTalesViewController: UIViewController {
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return .portrait
  }
  
  var backgroundImage: UIImageView!
  var backButton: UIButton!
  var titleLabel: UILabel!
  var tableView: UITableView!
  var pickataleCollectionView: UICollectionView!
  
  var dataSource: [Tale] = [Tale]()
  var mainTitle: String!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    setupViews()
    setupConstraints()
  }
  
  func setupViews() {
    backgroundImage = UIImageView()
    backgroundImage.image = appBackgroundImage
    
    backButton = UIButton()
    backButton.setBackgroundImage(UIImage(named: "btn_back.png"), for: .normal)
    backButton.backgroundColor = UIColor.clear
    backButton.addTarget(self, action: #selector(back(_:)), for: .touchUpInside)
    
    titleLabel = UILabel()
    titleLabel.textAlignment = .left
    titleLabel.adjustsFontSizeToFitWidth = true
    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont(name: "PTSans-Regular", size: 17.0)
    titleLabel.text = mainTitle
    
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    layout.itemSize = CGSize(width: (self.view.bounds.width).divided(by: 3.5), height: self.view.bounds.height.divided(by: 5))
    layout.scrollDirection = .vertical
    pickataleCollectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
    pickataleCollectionView.backgroundColor = UIColor.clear
    pickataleCollectionView.register(CardCollectionViewCell.self, forCellWithReuseIdentifier: "popularCell")
    pickataleCollectionView.dataSource = self
    pickataleCollectionView.delegate = self
    pickataleCollectionView.showsHorizontalScrollIndicator = false
    
    self.view.addSubview(backgroundImage)
    self.view.addSubview(backButton)
    self.view.addSubview(titleLabel)
    self.view.addSubview(pickataleCollectionView)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self.view)
    }
    
    backButton.snp.makeConstraints{ make in
      make.top.equalTo(self.view).offset(20)
      make.left.equalTo(self.view).offset(10)
      make.height.width.equalTo(self.view.frame.width/13)
    }
    
    titleLabel.snp.makeConstraints{ make in
      make.centerY.equalTo(backButton.snp.centerY)
      make.left.equalTo(backButton.snp.right).offset(10)
    }
    
    pickataleCollectionView.snp.makeConstraints{ make in
      make.left.equalTo(self.view).offset(10)
      make.right.equalTo(self.view).offset(-10)
      make.top.equalTo(titleLabel.snp.bottom).offset(20)
      make.bottom.equalTo(self.view).offset(-10)
    }
  }
  
  
  func back(_ sender: UIButton){
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

extension PresentTalesViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return dataSource.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
  {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "popularCell", for: indexPath)as! CardCollectionViewCell
    let tale = dataSource[indexPath.row]
    cell.setupCell(image: tale.imageName)
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let presentTaleVC = PresentTaleViewController()
    presentTaleVC.modalPresentationStyle = .overCurrentContext
    let tale = dataSource[indexPath.row]
    presentTaleVC.img = tale.imageName
    presentTaleVC.titleTale = tale.name
    presentTaleVC.descriptionTale = "Presentation of a nice tale..."
    self.present(presentTaleVC, animated: true, completion: nil)
  }
}

