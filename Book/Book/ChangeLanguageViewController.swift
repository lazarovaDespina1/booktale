//
//  ChangeLanguageViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//
import UIKit

class ChangeLanguageViewController: UIViewController {
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  var backgroundImage: UIImageView!
  var backButton: UIButton!
  var titleLabel: UILabel!
  var iconAppImage: UIImageView!
  var infoLabel: UILabel!
  var englishButton: UIButton!
  var chineseButton: UIButton!
  var saveButton: UIButton!
  
  var utils = Utils()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    setupViews()
    setupConstraints()
  }
  
  func setupViews() {
    backgroundImage = UIImageView()
    backgroundImage.image = UIImage(named: "Background")
    
    backButton = UIButton()
    backButton.setBackgroundImage(UIImage(named: "btn_back.png"), for: .normal)
    backButton.backgroundColor = UIColor.clear
    backButton.addTarget(self, action: #selector(back(_:)), for: .touchUpInside)
    
    iconAppImage = UIImageView()
    iconAppImage.contentMode = .scaleAspectFit
    iconAppImage.image = app_Icon
    iconAppImage.clipsToBounds = true
    
    titleLabel = UILabel()
    titleLabel.textAlignment = .center
    titleLabel.adjustsFontSizeToFitWidth = true
    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont(name: "PTSans-Regular", size: 16.0)
    titleLabel.text = "Pickatale"
    
    infoLabel = UILabel()
    infoLabel.textAlignment = .center
    infoLabel.adjustsFontSizeToFitWidth = true
    infoLabel.textColor = UIColor.white
    infoLabel.font = UIFont(name: "PTSans-Regular", size: 18.0)
    infoLabel.text = "Please select language you want the app and the book covers to appear in"
    infoLabel.numberOfLines = 0
    
    englishButton = UIButton()
    englishButton.titleLabel?.font = UIFont(name: "PTSans-Regular", size: 16.0)
    englishButton.setTitle("English", for: .normal)
    englishButton.setTitleColor(UIColor.white, for: .normal)
    englishButton.titleLabel?.textAlignment = .center
    englishButton.layer.cornerRadius = 15
    englishButton.backgroundColor = mainButtonColor
    
    chineseButton = UIButton()
    chineseButton.titleLabel?.font = UIFont(name: "PTSans-Regular", size: 16.0)
    chineseButton.setTitle("Chinese", for: .normal)
    chineseButton.setTitleColor(UIColor.white, for: .normal)
    chineseButton.titleLabel?.textAlignment = .center
    chineseButton.layer.cornerRadius = 15
    chineseButton.backgroundColor = mainButtonColor
    
    saveButton = UIButton()
    saveButton.titleLabel?.font = UIFont(name: "PTSans-Regular", size: 16.0)
    saveButton.setTitle("Save", for: .normal)
    saveButton.setTitleColor(UIColor.white, for: .normal)
    saveButton.titleLabel?.textAlignment = .center
    saveButton.layer.cornerRadius = 15
    saveButton.backgroundColor = mainButtonColor
    saveButton.addTarget(self, action: #selector(save(_:)), for: .touchUpInside)
    
    self.view.addSubview(backgroundImage)
    self.view.addSubview(backButton)
    self.view.addSubview(iconAppImage)
    self.view.addSubview(titleLabel)
    self.view.addSubview(infoLabel)
    self.view.addSubview(englishButton)
    self.view.addSubview(chineseButton)
    self.view.addSubview(saveButton)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self.view)
    }
    
    backButton.snp.makeConstraints{ make in
      make.top.equalTo(self.view).offset(20)
      make.left.equalTo(self.view).offset(20)
      make.height.width.equalTo(self.view.frame.width/13)
    }
    
    iconAppImage.snp.makeConstraints{ make in
      make.top.equalTo(self.view.frame.height/13)
      make.centerX.equalTo(self.view)
      make.width.height.equalTo(self.view.frame.width/3)
    }
    
    titleLabel.snp.makeConstraints{ make in
      make.top.equalTo(iconAppImage.snp.bottom).offset(20)
      make.centerX.equalTo(self.view)
    }
    
    infoLabel.snp.makeConstraints{ make in
      make.top.equalTo(titleLabel.snp.bottom).offset(10)
      make.centerX.equalTo(self.view)
      make.width.equalTo(self.view.bounds.width/2)
    }
    
    englishButton.snp.makeConstraints{ make in
      make.top.equalTo(infoLabel.snp.bottom).offset(20)
      make.centerX.equalTo(self.view)
      make.height.equalTo(50)
      make.width.equalTo(self.view.bounds.width/1.5)
    }
    
    chineseButton.snp.makeConstraints{ make in
      make.top.equalTo(englishButton.snp.bottom).offset(10)
      make.centerX.equalTo(self.view)
      make.height.equalTo(50)
      make.width.equalTo(self.view.bounds.width/1.5)
    }
    
    saveButton.snp.makeConstraints{ make in
      make.top.equalTo(chineseButton.snp.bottom).offset(30)
      make.centerX.equalTo(self.view)
      make.height.equalTo(50)
      make.width.equalTo(self.view.bounds.width/1.2)
    }
  }
  
  func save(_ sender: UIButton){
    var vc = SettingsViewController()
    if let navController = self.navigationController, navController.viewControllers.count >= 2 {
      vc = navController.viewControllers[navController.viewControllers.count - 2] as! SettingsViewController
    }
    vc.showAlert = true
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  func back(_ sender: UIButton){
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}
