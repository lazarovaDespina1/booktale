//
//  Constants.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import Foundation
import UIKit

let app_Icon = UIImage(named: "logoNew")
let appName = "BOOK TALES"
let appBackgroundImage = UIImage(named: "Background")
let appHeaderImage = UIImage(named: "header")

let mainButtonColor = UIColor(colorLiteralRed: 255 / 255, green: 91 / 255 , blue: 107 / 255, alpha: 1.0)
//let buttonColorTwo = UIColor(colorLiteralRed: 0 / 255, green: 96  / 255, blue: 255 / 255, alpha: 1.0) //old blue
let buttonColorTwo = UIColor(colorLiteralRed: 126 / 255, green: 84 / 255, blue: 143 / 255, alpha: 1.0)
let buttonColorThree = UIColor(colorLiteralRed: 50 / 255, green: 50  / 255, blue: 50 / 255, alpha: 1.0)



