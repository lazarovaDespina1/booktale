//
//  HomeViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return .portrait
  }
  
  var backgroundImage: UIImageView!
  var navigationView: NavigationView!
  var scrollView: UIScrollView!
  var timer: Timer! = nil
  var ageChoiceView: AgeChoiceView!
  var filterButton : UIButton!
  
  var pickataleLabel: UILabel!
  var pickataleButton: UIButton!
  var pickataleCollectionView: UICollectionView!
  
  var tableView: UITableView!
  var searchTableView: UITableView!
  
  var searchBar: UISearchBar!
  var searchActive : Bool = false
  var tap: UITapGestureRecognizer!
  
  var header:[String] = ["Booktale", "AudioBooks", "True Color", "Adventure & Action", "Animals"]
  var emptyHeader: [String] = [String]()
  var pageImages:[UIImage] = [UIImage(named: "home-1")!, UIImage(named: "Home2")!, UIImage(named: "Home3")!]
  var talesDataSource: [Tale] = [Tale]()
  var favTales: [Tale] = [Tale]()
  var dataSource: [Tale] = [Tale]()
  var filteredDataSource: [Tale] = [Tale]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    setupViews()
    setupConstraints()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(false)
  }
  
  func setupViews() {
    backgroundImage = UIImageView()
    backgroundImage.image = appBackgroundImage
    
    let url = Bundle.main.url(forResource: "AllTales", withExtension: "json")
    let data = try! Data(contentsOf: url!)
    let JSON = try! JSONSerialization.jsonObject(with: data, options: []) as! [[String:Any]]
    
    for item in JSON {
      let tale = Tale(data: item)
      talesDataSource.append(tale)
    }
    
    dataSource = talesDataSource
    filteredDataSource = talesDataSource
    emptyHeader = header
    
    navigationView = NavigationView()
    navigationView.delegate = self
    
    scrollView = UIScrollView(frame: CGRect(x:0, y:0, width:self.view.frame.size.width,height: self.view.frame.size.height.divided(by: 7)))
    var frame: CGRect = CGRect(x:0, y:0, width:0, height:0)
    scrollView.isPagingEnabled = true
    scrollView.contentSize = CGSize(width:self.scrollView.frame.size.width * 3, height: self.scrollView.frame.size.height)
    for index in 0..<pageImages.count {
      frame.origin.x = CGFloat(index) * scrollView.frame.size.width
      frame.size = self.scrollView.frame.size
      
      let imgViewRecipe = UIImageView(frame: frame)
      imgViewRecipe.contentMode = .scaleToFill
      imgViewRecipe.image = pageImages[index]
      
      self.scrollView.addSubview(imgViewRecipe)
    }
    scrollView.backgroundColor = .clear
    timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
    
    ageChoiceView = AgeChoiceView()
    
    filterButton = UIButton()
    filterButton.backgroundColor = .clear
    filterButton.setImage(UIImage(named: "controls"), for: .normal)
    filterButton.imageView?.contentMode = .scaleAspectFit
    filterButton.addTarget(self, action: #selector(presentFilterVC), for: .touchUpInside)
    
    tableView = UITableView()
    tableView.backgroundColor = UIColor.clear
    tableView.register(HomeTableViewCell.self, forCellReuseIdentifier: "cell")
    tableView.register(HeaderTableViewCell.self, forCellReuseIdentifier: "headerCell")
    tableView.dataSource = self
    tableView.delegate = self
    tableView.separatorStyle = .none
    
    searchBar = UISearchBar()
    searchBar.delegate = self
    searchBar.searchBarStyle = UISearchBarStyle.minimal
    searchBar.barTintColor = UIColor(colorLiteralRed: 34 / 255, green: 146 / 255 , blue: 189 / 255, alpha: 1.0)
    if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
      textfield.textColor = UIColor.white
      textfield.backgroundColor = .clear
      textfield.textAlignment = .left
    }
    searchBar.backgroundColor = .clear
    searchBar.placeholder = ""
    
    searchTableView = UITableView()
    searchTableView.backgroundColor = UIColor(colorLiteralRed: 233/255, green: 247/255, blue: 248/255, alpha: 0.8)
    searchTableView.register(SearchTableViewCell.self, forCellReuseIdentifier: "cellSearch")
    searchTableView.dataSource = self
    searchTableView.delegate = self
    searchTableView.isHidden = true
    
    tap = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.dismissKeyboard))
    tap.cancelsTouchesInView = false
    self.view.addGestureRecognizer(tap)
    
    let singleTap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
    scrollView.addGestureRecognizer(singleTap)
    scrollView?.showsHorizontalScrollIndicator = false
    
    self.view.addSubview(backgroundImage)
    self.view.addSubview(navigationView)
    self.view.addSubview(scrollView)
    self.view.addSubview(searchBar)
    self.view.addSubview(ageChoiceView)
    self.view.addSubview(filterButton)
    self.view.addSubview(tableView)
    self.view.addSubview(searchTableView)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self.view)
    }
    
    navigationView.snp.makeConstraints{ make in
      make.top.left.right.equalTo(self.view)
      make.height.equalTo(self.view.frame.width/5)
    }
    
    scrollView.snp.makeConstraints{ make in
      make.top.equalTo(navigationView.snp.bottom)
      make.left.right.equalTo(self.view)
      make.height.equalTo(self.view.frame.height/7)
    }
    
    searchBar.snp.makeConstraints{ make in
      make.top.equalTo(scrollView.snp.bottom)
      make.left.equalTo(self.view)
      make.width.equalTo(self.view.frame.width/2)
      make.height.equalTo(self.view.frame.height/13)
    }
    
    ageChoiceView.snp.makeConstraints{ make in
      make.top.equalTo(scrollView.snp.bottom)
      make.right.equalTo(filterButton.snp.left)
      make.left.equalTo(searchBar.snp.right)
      make.height.equalTo(self.view.frame.height/13)
    }
    
    filterButton.snp.makeConstraints{ make in
      make.top.equalTo(scrollView.snp.bottom)
      make.right.equalTo(self.view)
      make.height.equalTo(self.view.frame.height/13)
      make.width.equalTo(30)
    }
    
    tableView.snp.makeConstraints{ make in
      make.top.equalTo(ageChoiceView.snp.bottom)
      make.left.right.equalTo(self.view)
      make.bottom.equalTo(self.view)
    }
    
    searchTableView.snp.makeConstraints{ make in
      make.top.equalTo(ageChoiceView.snp.bottom)
      make.left.right.equalTo(self.view)
      make.bottom.equalTo(self.view)
    }
  }
  
  func handleTap(_ recognizer: UITapGestureRecognizer) {
    let screenWidth = UIScreen.main.bounds.width
    let recognizedTapLocation = recognizer.location(in: scrollView).x
    if 0...screenWidth ~= recognizedTapLocation{
      print("First")
      let presentVC = PresentTalesViewController()
      presentVC.mainTitle = "Explore Amazing Fairy Tales"
      presentVC.dataSource = talesDataSource
      self.navigationController?.pushViewController(presentVC, animated: false)
    } else if screenWidth...(screenWidth*2) ~= recognizedTapLocation {
      self.navigationController?.pushViewController(LogInViewController(), animated: false)
    } else if (screenWidth*2)...(screenWidth*3) ~= recognizedTapLocation {
      print("third")
      let presentVC = PresentTalesViewController()
      presentVC.mainTitle = "Best Picture Books"
      presentVC.dataSource = talesDataSource
      self.navigationController?.pushViewController(presentVC, animated: false)
    }
  }
  
  func moveToNextPage() {
    let pageWidth:CGFloat = self.scrollView.frame.width
    let maxWidth:CGFloat = pageWidth * 3
    let contentOffset:CGFloat = self.scrollView.contentOffset.x
    var slideToX = contentOffset + pageWidth
    
    if  contentOffset + pageWidth == maxWidth {
      slideToX = 0
    }
    self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:0, width:pageWidth, height:self.scrollView.frame.height), animated: true)
  }
  
  func presentFilterVC() {
    self.navigationController?.pushViewController(FilterViewController(), animated: true)
  }
  
  func dismissKeyboard() {
    view.endEditing(true)
  }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    if tableView == self.searchTableView{
      return 1
    } else {
      return emptyHeader.count
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if searchActive {
      return filteredDataSource.count
    } else {
      return 2
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if tableView == self.searchTableView {
      let tale = filteredDataSource[indexPath.row]
      let presentTaleVC = PresentTaleViewController()
      presentTaleVC.modalPresentationStyle = .overCurrentContext
      presentTaleVC.img = tale.imageName
      presentTaleVC.titleTale = tale.name
      presentTaleVC.descriptionTale = "There is a tale. One little tale."
      self.present(presentTaleVC, animated: true, completion: nil)
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if tableView == self.searchTableView {
      let cell = tableView.dequeueReusableCell(withIdentifier: "cellSearch", for: indexPath) as! SearchTableViewCell
      cell.backgroundColor = .clear
      cell.setupCell(title: filteredDataSource[indexPath.row].name, imgName: filteredDataSource[indexPath.row].imageName)
      cell.selectionStyle = .none
      return cell
    } else {
      if indexPath.row == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as! HeaderTableViewCell
        cell.backgroundColor = .clear
        cell.setupCell(title: header[indexPath.section])
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
      } else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell
        cell.backgroundColor = .clear
        cell.setupCell(tales: dataSource)
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
      }
    }
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if tableView == self.searchTableView {
      return 60
    } else {
      if indexPath.row == 0 {
        return 40
      } else {
        return (self.view.frame.height/4)
      }
    }
  }
}

extension HomeViewController: NavigationViewDelegate {
  func homeButtonTap(){
    dataSource = talesDataSource
    emptyHeader = header
    tableView.reloadData()
    navigationView.homeButton.imageView?.image = UIImage(named: "home")
    navigationView.favouritesButton.setImage(UIImage(named: "star"), for: .normal)
  }
  
  func favouriteButtonTap() {
    dataSource = favTales
    emptyHeader = [String]()
    tableView.reloadData()
    navigationView.homeButton.setImage(UIImage(named: "home"), for: .normal)
    navigationView.favouritesButton.setImage(UIImage(named: "star_pressed"), for: .normal)
  }
  
  func languageButtonTap() {
    self.navigationController?.pushViewController(ChooseLanguagesViewController(), animated: false)
  }
  
  func settingsButtonTap() {
    self.navigationController?.pushViewController(SettingsViewController(), animated: false)
  }
  
  func profileButtonTap() {
    self.navigationController?.pushViewController(LogInViewController(), animated: false)
  }
}

extension HomeViewController: HeaderTableViewCellDelegate {
  func presentTales(title: String) {
    let presentVC = PresentTalesViewController()
    presentVC.mainTitle = title
    presentVC.dataSource = talesDataSource
    
    self.navigationController?.pushViewController(presentVC, animated: false)
  }
}

extension HomeViewController: HomeTableViewCellDelegate {
  func presentTale(title: String, image: String, description: String) {
    let presentTaleVC = PresentTaleViewController()
    presentTaleVC.modalPresentationStyle = .overCurrentContext
    presentTaleVC.img = image
    presentTaleVC.titleTale = title
    presentTaleVC.descriptionTale = description
    self.present(presentTaleVC, animated: true, completion: nil)
  }
}

extension HomeViewController: UISearchBarDelegate {
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder() // hides the keyboard.
    searchActive = false
  }
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    searchActive = true
  }
  
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
    searchActive = false
    if searchBar.text! == ""{
      searchTableView.isHidden = true
    }
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    filteredDataSource = talesDataSource.filter { $0.name.hasPrefix(searchText) }
    self.searchTableView.reloadData()
    searchTableView.isHidden = false
  }
}

