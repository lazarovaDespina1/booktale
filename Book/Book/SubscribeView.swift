//
//  SubscribeView.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import Foundation
import UIKit

protocol SubscribeViewDelegate {
  func closeSubs()
}

class SubscribeView: UIView {
  var backgroundImage: UIImageView!
  var closeButton: UIButton!
  var appImage: UIImageView!
  var infoLabel: UILabel!
  
  var delegate: SubscribeViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    self.backgroundColor = UIColor.clear
    
    backgroundImage = UIImageView()
    backgroundImage.backgroundColor = UIColor(colorLiteralRed: 225 / 255, green: 53 / 255 , blue: 80 / 255, alpha: 1.0)
    backgroundImage.layer.cornerRadius = 10.0
    backgroundImage.clipsToBounds = true
    
    closeButton = UIButton()
    closeButton.backgroundColor = UIColor.clear
    closeButton.setImage(UIImage(named: "close"), for: .normal)
    closeButton.addTarget(self, action: #selector(close(_:)), for: .touchUpInside)
    
    appImage = UIImageView()
    appImage.contentMode = .scaleAspectFit
    appImage.image = app_Icon
    appImage.clipsToBounds = true
    
    infoLabel = UILabel()
    infoLabel.textAlignment = .center
    infoLabel.text = "Hold for 3 secounds to UNLOCK!"
    infoLabel.textColor = UIColor.white
    infoLabel.font = UIFont(name: "PTSans-Regular", size: 16.0)
    infoLabel.numberOfLines = 0
    
    self.addSubview(backgroundImage)
    self.addSubview(closeButton)
    self.addSubview(appImage)
    self.addSubview(infoLabel)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.top.left.right.bottom.equalTo(self)
    }
    
    closeButton.snp.makeConstraints{ make in
      make.top.equalTo(self).offset(20)
      make.right.equalTo(self).offset(-20)
      make.height.width.equalTo(30)
    }
    
    appImage.snp.makeConstraints{ make in
      make.top.equalTo(self).offset(30)
      make.centerX.equalTo(self)
      make.width.equalTo(UIScreen.main.bounds.width/4)
      make.height.equalTo(UIScreen.main.bounds.height/5)
    }
    
    infoLabel.snp.makeConstraints{ make in
      make.bottom.equalTo(self).offset(-20)
      make.left.right.equalTo(self).offset(20)
      make.right.equalTo(self).offset(-20)
      make.centerX.equalTo(self)
    }
  }
  
  func close(_ sender: UIButton) {
    delegate?.closeSubs()
  }
}
