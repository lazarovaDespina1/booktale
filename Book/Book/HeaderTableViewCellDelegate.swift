//
//  HeaderTableViewCellDelegate.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit

protocol HeaderTableViewCellDelegate
{
  func presentTales(title: String)
}

class HeaderTableViewCell: UITableViewCell {
  var titleLabel: UILabel!
  var openButton: UIButton!
  
  var delegate: HeaderTableViewCellDelegate?
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    titleLabel = UILabel()
    titleLabel.textColor = UIColor.white
    titleLabel.textAlignment = .left
    titleLabel.font = UIFont(name: "PTSans-Regular", size: 20.0)
    
    openButton = UIButton()
    openButton.backgroundColor = UIColor.clear
    openButton.setImage(UIImage(named: "plus"), for: .normal)
    openButton.addTarget(self, action: #selector(presentTap(_:)), for: .touchUpInside)
    
    contentView.addSubview(titleLabel)
    contentView.addSubview(openButton)
  }
  
  func setupConstraints() {
    titleLabel.snp.makeConstraints{ make in
      make.centerY.equalTo(self.contentView)
      make.left.equalTo(self.contentView).offset(20)
    }
    
    openButton.snp.makeConstraints{ make in
      make.centerY.equalTo(self.contentView)
      make.left.equalTo(titleLabel.snp.right).offset(20)
      make.height.width.equalTo(20)
    }
  }
  
  func setupCell(title: String) {
    titleLabel.text = title
  }
  
  func presentTap(_ sender: UIButton) {
    delegate?.presentTales(title: titleLabel.text!)
  }
}
