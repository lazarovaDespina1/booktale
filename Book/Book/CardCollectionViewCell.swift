//
//  CardCollectionViewCell.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
  var imageView: UIImageView!
  var downloadImageView: UIImageView!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupViews()
    setupConstraints()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    imageView = UIImageView()
    imageView.contentMode = .scaleAspectFill
    imageView.layer.cornerRadius = 10.0
    imageView.clipsToBounds = true
    
    downloadImageView = UIImageView()
    downloadImageView.contentMode = .scaleAspectFill
    downloadImageView.image = UIImage(named: "download")
    
    contentView.addSubview(imageView)
    contentView.addSubview(downloadImageView)
  }
  
  func setupConstraints() {
    imageView.snp.makeConstraints{ make in
      make.left.top.equalTo(self.contentView).offset(10)
      make.right.bottom.equalTo(self.contentView).offset(-10)
    }
    
    downloadImageView.snp.makeConstraints{ make in
      make.centerX.equalTo(self.contentView)
      make.bottom.equalTo(self.contentView).offset(-5)
      make.height.width.equalTo(20)
    }
  }
  
  func setupCell(image: String) {
    imageView.image = UIImage(named: image)
  }
}

