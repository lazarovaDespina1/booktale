//
//  SupportPageViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit

class SupportPageViewController: UIViewController {
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  var backgroundImage: UIImageView!
  var backButton: UIButton!
  var titleLabel: UILabel!
  var iconAppImage: UIImageView!
  var infoLabel: UILabel!
  var sendButton: UIButton!
  var emailLabel: UILabel!
  var buildLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    setupViews()
    setupConstraints()
  }
  
  func setupViews() {
    backgroundImage = UIImageView()
    backgroundImage.image = appBackgroundImage
    
    backButton = UIButton()
    backButton.setBackgroundImage(UIImage(named: "btn_back.png"), for: .normal)
    backButton.backgroundColor = UIColor.clear
    backButton.addTarget(self, action: #selector(back(_:)), for: .touchUpInside)
    
    titleLabel = UILabel()
    titleLabel.textAlignment = .center
    titleLabel.adjustsFontSizeToFitWidth = true
    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont(name: "PTSans-Regular", size: 20.0)
    titleLabel.text = "Settings"
    
    iconAppImage = UIImageView()
    iconAppImage.contentMode = .scaleAspectFit
    iconAppImage.image = UIImage(named: "AppIcon")
    iconAppImage.clipsToBounds = true
    
    infoLabel = UILabel()
    infoLabel.textAlignment = .center
    infoLabel.adjustsFontSizeToFitWidth = true
    infoLabel.textColor = UIColor.white
    infoLabel.font = UIFont(name: "PTSans-Regular", size: 16.0)
    infoLabel.text = "Our team is here to help! Lets's us know if yu need us or have ideas of how to make our product better."
    infoLabel.numberOfLines = 0
    
    sendButton = UIButton()
    sendButton.titleLabel?.font = UIFont(name: "PTSans-Regular", size: 16.0)
    sendButton.setTitle("Send email to Pickatale", for: .normal)
    sendButton.setTitleColor(UIColor.white, for: .normal)
    sendButton.titleLabel?.textAlignment = .center
    sendButton.layer.cornerRadius = 15
    sendButton.backgroundColor = mainButtonColor
    
    emailLabel = UILabel()
    emailLabel.textAlignment = .center
    emailLabel.adjustsFontSizeToFitWidth = true
    emailLabel.textColor = UIColor.white
    emailLabel.font = UIFont(name: "PTSans-Regular", size: 18.0)
    emailLabel.text = "www.pickatale.com"
    
    buildLabel = UILabel()
    buildLabel.textAlignment = .center
    buildLabel.adjustsFontSizeToFitWidth = true
    buildLabel.textColor = UIColor.white
    buildLabel.font = UIFont(name: "PTSans-Regular", size: 20.0)
    buildLabel.text = "Pickatale v.1.66 Build 128"
    
    self.view.addSubview(backgroundImage)
    self.view.addSubview(backButton)
    self.view.addSubview(iconAppImage)
    self.view.addSubview(titleLabel)
    self.view.addSubview(infoLabel)
    self.view.addSubview(sendButton)
    self.view.addSubview(emailLabel)
    self.view.addSubview(buildLabel)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self.view)
    }
    
    backButton.snp.makeConstraints{ make in
      make.top.equalTo(self.view).offset(20)
      make.left.equalTo(self.view).offset(20)
      make.height.width.equalTo(self.view.frame.width/13)
    }
    
    titleLabel.snp.makeConstraints{ make in
      make.centerY.equalTo(backButton.snp.centerY)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
    }
    
    iconAppImage.snp.makeConstraints{ make in
      make.top.equalTo(titleLabel.snp.bottom).offset(40)
      make.centerX.equalTo(self.view)
      make.width.height.equalTo(self.view.frame.width/3)
    }
    
    infoLabel.snp.makeConstraints{ make in
      make.top.equalTo(iconAppImage.snp.bottom).offset(20)
      make.centerX.equalTo(self.view)
      make.left.equalTo(self.view).offset(30)
      make.right.equalTo(self.view).offset(-30)
    }
    
    sendButton.snp.makeConstraints{ make in
      make.top.equalTo(infoLabel.snp.bottom).offset(30)
      make.centerX.equalTo(self.view)
      make.height.equalTo(50)
      make.width.equalTo(self.view.bounds.width/1.5)
    }
    
    buildLabel.snp.makeConstraints{ make in
      make.bottom.equalTo(self.view).offset(-10)
      make.centerX.equalTo(self.view)
    }
    
    emailLabel.snp.makeConstraints{ make in
      make.bottom.equalTo(buildLabel.snp.top).offset(-20)
      make.centerX.equalTo(self.view)
    }
  }
  
  func sendEmail(_ sender: UIButton){
  }
  
  func back(_ sender: UIButton){
    
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

