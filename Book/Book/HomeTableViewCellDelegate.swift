//
//  HomeTableViewCellDelegate.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit

protocol HomeTableViewCellDelegate {
  func presentTale(title: String, image: String, description: String)
}

class HomeTableViewCell: UITableViewCell {
  
  var pickataleCollectionView: UICollectionView!
  var dataSource: [Tale] = [Tale]()
  var delegate: HomeTableViewCellDelegate?
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    let screenSize = UIScreen.main.bounds
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    layout.itemSize = CGSize(width: (screenSize.width).divided(by: 3), height: screenSize.height.divided(by: 4))
    layout.scrollDirection = .horizontal
    pickataleCollectionView = UICollectionView(frame: self.bounds, collectionViewLayout: layout)
    pickataleCollectionView.backgroundColor = UIColor.clear
    pickataleCollectionView.register(CardCollectionViewCell.self, forCellWithReuseIdentifier: "popularCell")
    pickataleCollectionView.dataSource = self
    pickataleCollectionView.delegate = self
    pickataleCollectionView.showsHorizontalScrollIndicator = false
    
    contentView.addSubview(pickataleCollectionView)
  }
  
  func setupConstraints() {
    pickataleCollectionView.snp.makeConstraints{ make in
      make.left.equalTo(self.contentView)
      make.bottom.top.right.equalTo(self.contentView)
    }
  }
  
  func setupCell(tales: [Tale]) {
    self.dataSource = tales
    pickataleCollectionView.reloadData()
  }
}

extension HomeTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return dataSource.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
  {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "popularCell", for: indexPath)as! CardCollectionViewCell
    let tale = dataSource[indexPath.row]
    cell.setupCell(image: tale.imageName)
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let tale = dataSource[indexPath.row]
    delegate?.presentTale(title: tale.name, image: tale.imageName, description: "This is a tale description. Have4 a good day Sir.")
  }
}

