//
//  LandscapeAVPlayerController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import AVKit

class LandscapeAVPlayerController: AVPlayerViewController {
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return .landscapeLeft
  }
}
