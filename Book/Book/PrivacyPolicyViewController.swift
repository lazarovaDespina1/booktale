//
//  PrivacyPolicyViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController {
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  var backgroundImage: UIImageView!
  var backButton: UIButton!
  var titleLabel: UILabel!
  var smallTitleLabel: UILabel!
  var textView: UITextView!
  
  var titleForLabel = ""
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    setupViews()
    setupConstraints()
  }
  
  func setupViews() {
    backgroundImage = UIImageView()
    backgroundImage.image = appBackgroundImage
    
    backButton = UIButton()
    backButton.setBackgroundImage(UIImage(named: "btn_back.png"), for: .normal)
    backButton.backgroundColor = UIColor.clear
    backButton.addTarget(self, action: #selector(back(_:)), for: .touchUpInside)
    
    titleLabel = UILabel()
    titleLabel.textAlignment = .center
    titleLabel.adjustsFontSizeToFitWidth = true
    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont(name: "PTSans-Regular", size: 20.0)
    titleLabel.text = titleForLabel
    
    smallTitleLabel = UILabel()
    smallTitleLabel.textAlignment = .center
    smallTitleLabel.adjustsFontSizeToFitWidth = true
    smallTitleLabel.textColor = UIColor.white
    smallTitleLabel.font = UIFont(name: "PTSans-Regular", size: 10.0)
    smallTitleLabel.text = "Booktale"
    
    textView = UITextView()
    
    textView.backgroundColor = UIColor.clear
    textView.font = UIFont(name: "PTSans-Regular", size: 20.0)
    textView.textColor = .white
    textView.textAlignment = .justified
    textView.text = "1. Introduction \n Thanks for choosing BookTale. To provide you with the Booktale Service (or Service, both as defined in the Terms and Conditions of Use), Booktale (Booktale, we, us, our) needs to collect some information about you, and this Privacy Policy is where we describe the information we collect, what we do with it, and how you can manage and control the use of your information. By using or interacting with the Service, you are entering into a binding contract with us and consenting to the use of your information as explained in this Privacy Policy. If you don't agree with these terms, then please don't use the Service.\n\n 2. The information we collect \n\n2.1 Registration data \n When you sign up for the Service, we may collect information we ask you for, like e-mail address, password, date of birth, gender, billing information, and country. We may also collect information you voluntarily add to your profile, such as your mobile phone number. If you connect to the Service using your Facebook credentials, you authorise us to collect your authentication information, such as your username, encrypted access credentials, and other information that may be available on or through your Facebook account, including your name, profile picture, country, hometown, e-mail address, date of birth, gender, friends' names and profile pictures and networks. We may store this information so that it can be used for the purposes explained in Section 3 and may verify your credentials with Facebook\n\n1. Introduction \n Thanks for choosing Booktale. To provide you with the Booktale Service (or Service, both as defined in the Terms and Conditions of Use), Booktale (Booktale, we, us, our) needs to collect some information about you, and this Privacy Policy is where we describe the information we collect, what we do with it, and how you can manage and control the use of your information. By using or interacting with the Service, you are entering into a binding contract with us and consenting to the use of your information as explained in this Privacy Policy. If you don't agree with these terms, then please don't use the Service.\n\n 2. The information we collect \n\n2.1 Registration data \n When you sign up for the Service, we may collect information we ask you for, like e-mail address, password, date of birth, gender, billing information, and country. We may also collect information you voluntarily add to your profile, such as your mobile phone number. If you connect to the Service using your Facebook credentials, you authorise us to collect your authentication information, such as your username, encrypted access credentials, and other information that may be available on or through your Facebook account, including your name, profile picture, country, hometown, e-mail address, date of birth, gender, friends' names and profile pictures and networks. We may store this information so that it can be used for the purposes explained in Section 3 and may verify your credentials with Facebook"
    textView.isEditable = false
    
    self.view.addSubview(backgroundImage)
    self.view.addSubview(backButton)
    self.view.addSubview(titleLabel)
    self.view.addSubview(smallTitleLabel)
    self.view.addSubview(textView)
    
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self.view)
    }
    
    backButton.snp.makeConstraints{ make in
      make.top.equalTo(self.view).offset(20)
      make.left.equalTo(self.view).offset(20)
      make.height.width.equalTo(self.view.frame.width/13)
    }
    
    titleLabel.snp.makeConstraints{ make in
      make.centerY.equalTo(backButton.snp.centerY)
      make.left.equalTo(self.view)
      make.right.equalTo(self.view)
    }
    
    smallTitleLabel.snp.makeConstraints{ make in
      make.top.equalTo(titleLabel.snp.bottom)
      make.centerX.equalTo(self.view)
    }
    
    textView.snp.makeConstraints{ make in
      make.left.right.bottom.equalTo(self.view)
      make.top.equalTo(smallTitleLabel.snp.bottom)
    }
  }
  
  func sendEmail(_ sender: UIButton){
  }
  
  func back(_ sender: UIButton){
    
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}
