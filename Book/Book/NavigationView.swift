//
//  NavigationView.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import Foundation
import SnapKit

protocol NavigationViewDelegate {
  func homeButtonTap()
  func favouriteButtonTap()
  func languageButtonTap()
  func settingsButtonTap()
  func profileButtonTap()
}

class NavigationView: UIView {
  
  var backgroundImage: UIImageView!
  var titleLabel: UILabel!
  var homeButton : UIButton!
  var favouritesButton : UIButton!
  var languageButton : UIButton!
  var settingsButton : UIButton!
  var profileButton : UIButton!
  
  var delegate: NavigationViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    //self.backgroundColor = mainButtonColor
    backgroundImage = UIImageView()
    backgroundImage.image = appHeaderImage
    
    titleLabel = UILabel()
    titleLabel.text = "BookTales"
    titleLabel.textAlignment = .left
    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont(name: "PTSans-Bold", size: 20.0)!
    
    homeButton = UIButton()
    homeButton.backgroundColor = UIColor.clear
    homeButton.setImage(UIImage(named: "home"), for: .normal)
    homeButton.addTarget(self, action: #selector(homeTap(_:)), for: .touchUpInside)
    
    favouritesButton = UIButton()
    favouritesButton.backgroundColor = UIColor.clear
    favouritesButton.setImage(UIImage(named: "star"), for: .normal)
    favouritesButton.addTarget(self, action: #selector(favouriteTap(_:)), for: .touchUpInside)
    
    languageButton = UIButton()
    languageButton.backgroundColor = UIColor.clear
    languageButton.setImage(UIImage(named: "shopping-cart"), for: .normal)
    languageButton.addTarget(self, action: #selector(languageTap(_:)), for: .touchUpInside)
    
    settingsButton = UIButton()
    settingsButton.backgroundColor = UIColor.clear
    settingsButton.setImage(UIImage(named: "settings"), for: .normal)
    settingsButton.addTarget(self, action: #selector(settingsTap(_:)), for: .touchUpInside)
    
    profileButton = UIButton()
    profileButton.backgroundColor = UIColor.clear
    profileButton.setImage(UIImage(named: "Profile"), for: .normal)
    profileButton.addTarget(self, action: #selector(profileTap(_:)), for: .touchUpInside)
    
    self.addSubview(backgroundImage)
    self.addSubview(profileButton)
    self.addSubview(settingsButton)
    self.addSubview(languageButton)
    self.addSubview(favouritesButton)
    self.addSubview(homeButton)
    self.addSubview(titleLabel)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self)
    }
    
    profileButton.snp.makeConstraints{ make in
      make.centerY.equalTo(self)
      make.right.equalTo(self).offset(-10)
      make.width.height.equalTo(40)
    }
    
    settingsButton.snp.makeConstraints{ make in
      make.centerY.equalTo(self)
      make.right.equalTo(profileButton.snp.left).offset(-10)
      make.width.height.equalTo(30)
    }
    
    languageButton.snp.makeConstraints{ make in
      make.centerY.equalTo(self)
      make.right.equalTo(settingsButton.snp.left).offset(-10)
      make.width.height.equalTo(30)
    }
    
    favouritesButton.snp.makeConstraints{ make in
      make.centerY.equalTo(self)
      make.right.equalTo(languageButton.snp.left).offset(-10)
      make.width.height.equalTo(30)
    }
    
    homeButton.snp.makeConstraints{ make in
      make.centerY.equalTo(self)
      make.right.equalTo(favouritesButton.snp.left).offset(-10)
      make.width.height.equalTo(30)
    }
    
    titleLabel.snp.makeConstraints{ make in
      make.centerY.equalTo(self)
      make.right.equalTo(homeButton.snp.left).offset(-10)
      make.left.equalTo(self).offset(10)
    }
  }
  
  func homeTap(_ sender: UIButton) {
    delegate?.homeButtonTap()
  }
  
  func favouriteTap(_ sender: UIButton) {
    delegate?.favouriteButtonTap()
  }
  
  func languageTap(_ sender: UIButton) {
    delegate?.languageButtonTap()
  }
  
  func settingsTap(_ sender: UIButton) {
    delegate?.settingsButtonTap()
  }
  
  func profileTap(_ sender: UIButton) {
    delegate?.profileButtonTap()
  }
}
