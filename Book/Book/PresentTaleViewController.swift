//
//  PresentTaleViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PresentTaleViewController: UIViewController, UIGestureRecognizerDelegate, AVPlayerViewControllerDelegate {
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return .portrait
  }

  let playerController = LandscapeAVPlayerController()
  var player = AVPlayer()
  
  var presentView: PresentTaleView!
  var titleTale: String!
  var img: String!
  var descriptionTale: String!
  var subscribeView: SubscribeView!
  
  var timer:Timer! = nil
  var longGesture:UILongPressGestureRecognizer! = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    setuConstraints()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    let value = UIInterfaceOrientation.portrait.rawValue
    UIDevice.current.setValue(value, forKey: "orientation")
  }
  
  func setupViews() {
    self.view.backgroundColor = UIColor(colorLiteralRed: 48/255, green: 48/255, blue: 48/255, alpha: 0.6)
    
    presentView = PresentTaleView()
    presentView.delegate = self
    presentView.infoLabel.text = descriptionTale
    presentView.taleImage.image = UIImage(named: img)
    presentView.taleNameLabel.text = titleTale
    
    subscribeView = SubscribeView()
    subscribeView.isHidden = true
    subscribeView.delegate = self
    
    longGesture = UILongPressGestureRecognizer()
    longGesture.minimumPressDuration = 0.1
    longGesture.numberOfTouchesRequired = 1
    longGesture.addTarget(self, action: #selector(doTouchActions))
    subscribeView.addGestureRecognizer(longGesture)
    
    self.view.addSubview(presentView)
    self.view.addSubview(subscribeView)
  }
  
  func setuConstraints() {
    presentView.snp.makeConstraints{ make in
      make.left.equalTo(self.view).offset(30)
      make.right.equalTo(self.view).offset(-30)
      make.top.equalTo(self.view).offset(60)
      make.bottom.equalTo(self.view).offset(-60)
    }
    
    subscribeView.snp.makeConstraints{ make in
      make.left.equalTo(self.view).offset(20)
      make.right.equalTo(self.view).offset(-20)
      make.centerY.equalTo(self.view)
      make.height.equalTo(self.view.frame.height/3)
    }
  }
  
  func createTimer() {
    if timer == nil {
      timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(cancelTrouchFromTimer), userInfo: nil, repeats: false)
    }
  }
  
  func cancelTimer() {
    if timer != nil {
      timer.invalidate()
      timer = nil
      subscribeView.infoLabel.textColor = .white
    }
  }
  
  func cancelTrouchFromTimer() {
    cancelTimer()
    let bundle = Bundle.main
    let moviePath: String? = bundle.path(forResource: "TaleVid", ofType: "MOV")
    let movieURL = URL(fileURLWithPath: moviePath!)
    player = AVPlayer(playerItem: AVPlayerItem(url: movieURL))
    
    playerController.delegate = self
    playerController.player = player
    playerController.showsPlaybackControls = false
    player.play()
    self.present(playerController, animated: false) {
      NotificationCenter.default.addObserver(self, selector: #selector(PresentTaleViewController.videoFinish), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: self.player.currentItem)
    }
  }
  
  func videoFinish() {
    player.pause()
    subscribeView.isHidden = true
   // subscribeView.infoLabel.textColor = .white
    playerController.dismiss(animated: false, completion: nil)
  }
  
  func doTouchActions(_ sender: UILongPressGestureRecognizer) {
    if sender.state == .began {
      subscribeView.infoLabel.textColor = .gray
      createTimer()
    }
    if sender.state == .ended {
      cancelTimer()
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

extension PresentTaleViewController: PresentTaleViewDelegate {
  func close(){
    self.dismiss(animated: true, completion: nil)
    
  }
  
  func subscribe(){
    subscribeView.isHidden = false
  }
}


extension PresentTaleViewController: SubscribeViewDelegate {
  func closeSubs(){
    subscribeView.isHidden = true
  }
}

