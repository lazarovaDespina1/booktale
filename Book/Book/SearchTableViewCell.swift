//
//  SearchTableViewCell.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
  var titleLabel: UILabel!
  var taleImage: UIImageView!
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    taleImage = UIImageView()
    taleImage.contentMode = .scaleAspectFit
    taleImage.layer.cornerRadius = 10.0
    taleImage.clipsToBounds = true
    
    titleLabel = UILabel()
    titleLabel.textAlignment = .left
    titleLabel.textColor = UIColor(colorLiteralRed: 34 / 255, green: 146 / 255 , blue: 189 / 255, alpha: 1.0)
    
    contentView.addSubview(taleImage)
    contentView.addSubview(titleLabel)
  }
  
  func setupConstraints() {
    taleImage.snp.makeConstraints{ make in
      make.left.equalTo(self.contentView).offset(20)
      make.bottom.equalTo(self.contentView).offset(-5)
      make.top.equalTo(self.contentView).offset(5)
      make.width.equalTo(self.contentView.snp.height)
    }
    
    titleLabel.snp.makeConstraints{ make in
      make.right.bottom.top.equalTo(self.contentView)
      make.left.equalTo(taleImage.snp.right).offset(10)
    }
  }
  
  func setupCell(title: String, imgName: String) {
    titleLabel?.text = title
    taleImage.image = UIImage(named: imgName)
  }
}
