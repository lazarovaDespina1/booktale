//
//  SettingsViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit
import Foundation
import SnapKit

class SettingsViewController: UIViewController {
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return .portrait
  }
  
  var backgroundImage: UIImageView!
  var backButton: UIButton!
  var titleLabel: UILabel!
  var tableView: UITableView!
  var languageButton: UIButton!
  var languageLabel: UILabel!
  
  var data = ["Support Page", "How it works", "Privacy Policy", "Terms And Conditions"]
  var images = ["support", "works", "privacy", "terms"]
  
  var showAlert = false
  var utils = Utils()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    setupViews()
    setupConstraints()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    if showAlert {
      self.present(utils.nativeAllertAction(message: "Success"), animated: true, completion: nil)
    }
    showAlert = false
  }
  
  func setupViews() {
    backgroundImage = UIImageView()
    backgroundImage.image = appBackgroundImage
    
    backButton = UIButton()
    backButton.setBackgroundImage(UIImage(named: "btn_back.png"), for: .normal)
    backButton.backgroundColor = UIColor.clear
    backButton.addTarget(self, action: #selector(back(_:)), for: .touchUpInside)
    
    titleLabel = UILabel()
    titleLabel.textAlignment = .center
    titleLabel.adjustsFontSizeToFitWidth = true
    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont(name: "PTSans-Regular", size: 20.0)
    titleLabel.text = "Settings"
    
    languageButton = UIButton()
    languageButton.titleLabel?.font = UIFont(name: "PTSans-Regular", size: 16.0)
    languageButton.setBackgroundImage(UIImage(named: "language"), for: .normal)
    languageButton.backgroundColor = UIColor.clear
    languageButton.addTarget(self, action: #selector(changeLanguage(_:)), for: .touchUpInside)
    
    languageLabel = UILabel()
    languageLabel.textAlignment = .center
    languageLabel.adjustsFontSizeToFitWidth = true
    languageLabel.textColor = UIColor.white
    languageLabel.font = UIFont(name: "PTSans-Regular", size: 15.0)
    languageLabel.text = "Switch Language"
    
    tableView = UITableView()
    tableView.backgroundColor = UIColor.clear
    tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: "cell")
    tableView.dataSource = self
    tableView.delegate = self
    
    self.view.addSubview(backgroundImage)
    self.view.addSubview(backButton)
    self.view.addSubview(titleLabel)
    self.view.addSubview(languageButton)
    self.view.addSubview(languageLabel)
    self.view.addSubview(tableView)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self.view)
    }
    
    backButton.snp.makeConstraints{ make in
      make.top.equalTo(self.view).offset(20)
      make.left.equalTo(self.view).offset(20)
      make.height.width.equalTo(self.view.frame.width/13)
    }
    
    titleLabel.snp.makeConstraints{ make in
      make.centerY.equalTo(backButton.snp.centerY)
      make.left.equalTo(backButton.snp.right).offset(10)
    }
    
    languageButton.snp.makeConstraints{ make in
      make.top.equalTo(titleLabel.snp.bottom).offset(40)
      make.centerX.equalTo(self.view)
      make.height.width.equalTo(50)
    }
    
    languageLabel.snp.makeConstraints{ make in
      make.top.equalTo(languageButton.snp.bottom)
      make.centerX.equalTo(self.view)
    }
    
    tableView.snp.makeConstraints{ make in
      make.left.equalTo(self.view).offset(10)
      make.right.equalTo(self.view).offset(-10)
      make.top.equalTo(languageLabel.snp.bottom).offset(20)
      make.height.equalTo(64 * data.count)
    }
  }
  
  
  func back(_ sender: UIButton){
    _ = self.navigationController?.popViewController(animated: false)
  }
  
  func changeLanguage(_ sender: UIButton) {
    self.navigationController?.pushViewController(ChangeLanguageViewController(), animated: false)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return data.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SettingsTableViewCell
    
    cell.setupCell(image: images[indexPath.row], text: data[indexPath.row])
    cell.backgroundColor = .clear
    cell.selectionStyle = .none
    cell.delegate = self
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 64
  }
}

extension SettingsViewController: SettingsTableViewCellDelegate {
  func presentNextVC(title: String) {
    switch title {
    case "Support Page":
      self.navigationController?.pushViewController(SupportPageViewController(), animated: false)
    case "How it works":
      self.navigationController?.pushViewController(HowItWorksViewController(), animated: false)
    case "Privacy Policy":
      let privacyVC = PrivacyPolicyViewController()
      privacyVC.titleForLabel = "Privacy Policy"
      self.navigationController?.pushViewController(privacyVC, animated: false)
    case "Terms And Conditions":
      let privacyVC = PrivacyPolicyViewController()
      privacyVC.titleForLabel = "Terms And Conditions"
      self.navigationController?.pushViewController(privacyVC, animated: false)
    default:
      break
    }
  }
}

