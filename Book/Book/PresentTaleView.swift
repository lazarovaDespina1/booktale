//
//  PresentTaleView.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import Foundation
import UIKit

protocol PresentTaleViewDelegate {
  func close()
  func subscribe()
}

class PresentTaleView: UIView {
  var backgroundImage: UIImageView!
  var closeButton: UIButton!
  var taleImage: UIImageView!
  var taleNameLabel: UILabel!
  var infoLabel: UILabel!
  var subscribeButton: UIButton!
  
  var delegate: PresentTaleViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    self.backgroundColor = UIColor.clear
    
    backgroundImage = UIImageView()
    backgroundImage.image = appBackgroundImage
    backgroundImage.layer.cornerRadius = 10.0
    backgroundImage.clipsToBounds = true
    
    closeButton = UIButton()
    closeButton.backgroundColor = UIColor.clear
    closeButton.setImage(UIImage(named: "close"), for: .normal)
    closeButton.addTarget(self, action: #selector(close(_:)), for: .touchUpInside)
    
    taleImage = UIImageView()
    taleImage.contentMode = .scaleAspectFill
    taleImage.layer.cornerRadius = 10.0
    taleImage.clipsToBounds = true
    
    taleNameLabel = UILabel()
    taleNameLabel.textAlignment = .center
    taleNameLabel.adjustsFontSizeToFitWidth = true
    taleNameLabel.textColor = UIColor.white
    taleNameLabel.font = UIFont(name: "PTSans-Regular", size: 20.0)
    
    infoLabel = UILabel()
    infoLabel.textAlignment = .center
    infoLabel.textColor = UIColor.white
    infoLabel.font = UIFont(name: "PTSans-Regular", size: 20.0)
    infoLabel.numberOfLines = 0
    
    subscribeButton = UIButton()
    subscribeButton.setTitle("Subscribe", for: .normal)
    subscribeButton.setTitleColor(UIColor.white, for: .normal)
    subscribeButton.titleLabel?.textAlignment = .center
    subscribeButton.layer.cornerRadius = 15
    subscribeButton.titleLabel?.font = UIFont(name: "PTSans-Regular", size: 20.0)
    subscribeButton.backgroundColor = UIColor(colorLiteralRed: 225 / 255, green: 53 / 255 , blue: 80 / 255, alpha: 1.0)
    subscribeButton.addTarget(self, action: #selector(subscribe(_:)), for: .touchUpInside)
    
    self.addSubview(backgroundImage)
    self.addSubview(closeButton)
    self.addSubview(taleImage)
    self.addSubview(taleNameLabel)
    self.addSubview(infoLabel)
    self.addSubview(subscribeButton)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.top.left.right.bottom.equalTo(self)
    }
    
    closeButton.snp.makeConstraints{ make in
      make.top.equalTo(self).offset(20)
      make.right.equalTo(self).offset(-20)
      make.height.width.equalTo(30)
    }
    
    taleImage.snp.makeConstraints{ make in
      make.top.equalTo(self).offset(30)
      make.centerX.equalTo(self)
      make.width.equalTo(UIScreen.main.bounds.width/2)
      make.height.equalTo(UIScreen.main.bounds.height/2.5)
    }
    
    taleNameLabel.snp.makeConstraints{ make in
      make.top.equalTo(taleImage.snp.bottom).offset(10)
      make.centerX.equalTo(self)
    }
    
    infoLabel.snp.makeConstraints{ make in
      make.top.equalTo(taleNameLabel.snp.bottom).offset(30)
      make.left.right.equalTo(self).offset(20)
      make.right.equalTo(self).offset(-20)
      make.centerX.equalTo(self)
    }
    
    subscribeButton.snp.makeConstraints{ make in
      make.bottom.equalTo(self).offset(-20)
      make.left.equalTo(self).offset(20)
      make.right.equalTo(self).offset(-20)
      make.height.equalTo(50)
    }
  }
  
  func close(_ sender: UIButton) {
    delegate?.close()
  }
  
  func subscribe(_ sender: UIButton) {
    delegate?.subscribe()
  }
}
