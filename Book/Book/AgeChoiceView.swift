//
//  AgeChoiceView.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import Foundation
import UIKit

class AgeChoiceView: UIView {
  var zeroToTwoButton : UIButton!
  var twoToFourButton : UIButton!
  var fourToSixButton : UIButton!
  var sixToEghtButton : UIButton!
  
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    zeroToTwoButton = createButton(age: "0-2")
    zeroToTwoButton.addTarget(self, action: #selector(changeColour(_:)), for: .touchUpInside)
    
    twoToFourButton = createButton(age: "2-4")
    twoToFourButton.addTarget(self, action: #selector(changeColour(_:)), for: .touchUpInside)
    
    fourToSixButton = createButton(age: "4-6")
    fourToSixButton.addTarget(self, action: #selector(changeColour(_:)), for: .touchUpInside)
    
    sixToEghtButton = createButton(age: "6-8")
    sixToEghtButton.addTarget(self, action: #selector(changeColour(_:)), for: .touchUpInside)
    
    self.addSubview(sixToEghtButton)
    self.addSubview(fourToSixButton)
    self.addSubview(twoToFourButton)
    self.addSubview(zeroToTwoButton)
  }
  
  func setupConstraints() {
    sixToEghtButton.snp.makeConstraints{ make in
      make.centerY.equalTo(self)
      make.right.equalTo(self).offset(-8)
      make.width.height.equalTo(30)
    }
    
    fourToSixButton.snp.makeConstraints{ make in
      make.centerY.equalTo(self)
      make.right.equalTo(sixToEghtButton.snp.left).offset(-8)
      make.width.height.equalTo(30)
    }
    
    twoToFourButton.snp.makeConstraints{ make in
      make.centerY.equalTo(self)
      make.right.equalTo(fourToSixButton.snp.left).offset(-8)
      make.width.height.equalTo(30)
    }
    
    zeroToTwoButton.snp.makeConstraints{ make in
      make.centerY.equalTo(self)
      make.right.equalTo(twoToFourButton.snp.left).offset(-8)
      make.width.height.equalTo(30)
    }
  }
  
  func createButton(age: String) -> UIButton {
    let button = UIButton()
    button.backgroundColor = UIColor.clear
    button.setTitle(age, for: .normal)
    button.setTitleColor(UIColor.white, for: .normal)
    button.titleLabel?.textAlignment = .center
    button.titleLabel?.font = UIFont(name: "PTSans-Bold", size: 13.0)
    return button
  }
  
  func changeColour(_ sender: UIButton) {
    if sender.backgroundColor == UIColor(colorLiteralRed: 255 / 255, green: 179 / 255, blue: 13 / 255, alpha: 1.0) {
      sender.backgroundColor = .clear
    } else {
      sender.backgroundColor = UIColor(colorLiteralRed: 255 / 255, green: 179 / 255, blue: 13 / 255, alpha: 1.0)
    }
  }
}
