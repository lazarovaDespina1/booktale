//
//  SettingsTableViewCell.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit

protocol SettingsTableViewCellDelegate
{
  func presentNextVC(title: String)
}

class SettingsTableViewCell: UITableViewCell {
  var titleLabel: UILabel!
  var leftImageView: UIImageView!
  var nextButton: UIButton!
  
  var delegate: SettingsTableViewCellDelegate?
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupViews()
    setupConstraints()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setupViews() {
    contentView.backgroundColor = UIColor.clear
    
    titleLabel = UILabel()
    titleLabel.textColor = UIColor.white
    titleLabel.textAlignment = .left
    titleLabel.font = UIFont(name: "PTSans-Bold", size: 20.0)
    
    leftImageView = UIImageView()
    leftImageView.contentMode = .scaleAspectFit
    
    nextButton = UIButton()
    nextButton.setBackgroundImage(UIImage(named: "right-arrow"), for: .normal)
    nextButton.backgroundColor = UIColor.clear
    nextButton.addTarget(self, action: #selector(presentTap(_:)), for: .touchUpInside)
    
    contentView.addSubview(leftImageView)
    contentView.addSubview(titleLabel)
    contentView.addSubview(nextButton)
  }
  
  func setupConstraints() {
    leftImageView.snp.makeConstraints{ make in
      make.centerY.equalTo(contentView)
      make.left.equalTo(contentView).offset(30)
      make.width.height.equalTo(contentView.snp.width).dividedBy(7)
    }
    
    titleLabel.snp.makeConstraints{ make in
      make.centerY.equalTo(contentView)
      make.left.equalTo(leftImageView.snp.right).offset(10)
      make.right.equalTo(contentView)
    }
    
    nextButton.snp.makeConstraints{ make in
      make.centerY.equalTo(contentView)
      make.right.equalTo(contentView.snp.right).offset(-20)
      make.width.height.equalTo(contentView.snp.height).dividedBy(2)
    }
  }
  
  func setupCell(image: String, text: String) {
    self.leftImageView.image = UIImage(named: image)
    self.titleLabel.text = text
  }
  
  func presentTap(_ sender: UIButton) {
    delegate?.presentNextVC(title: titleLabel.text!)
  }
}

