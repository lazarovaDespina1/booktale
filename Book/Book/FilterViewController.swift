//
//  FilterViewController.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import UIKit
import Foundation
import SnapKit

class FilterViewController: UIViewController {
  override var prefersStatusBarHidden: Bool {
    return true
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    return .portrait
  }
  
  var backgroundImage: UIImageView!
  var backButton: UIButton!
  var titleLabel: UILabel!
  
  var ageLabel: UILabel!
  var zeroToTwoButton : UIButton!
  var twoToFourButton : UIButton!
  var fourToSixButton : UIButton!
  var sixToEghtButton : UIButton!
  
  var categotiesLabel: UILabel!
  var adventureView: UIView!
  var animalView: UIView!
  var classicView: UIView!
  var craftView: UIView!
  var fairyView: UIView!
  var familyView: UIView!
  var fantasyView: UIView!
  var funView: UIView!
  var learningView: UIView!
  
  var collectionLabel: UILabel!
  var pickataleButton: UIButton!
  var audiobooksButton: UIButton!
  var truecolorButton: UIButton!
  
  var applyFilterButton: UIButton!
  
  var talesDataSource: [Tale] = [Tale]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
    setupViews()
    setupConstraints()
  }
  
  func setupViews() {
    backgroundImage = UIImageView()
    backgroundImage.image = appBackgroundImage
    
    let url = Bundle.main.url(forResource: "AllTales", withExtension: "json")
    let data = try! Data(contentsOf: url!)
    let JSON = try! JSONSerialization.jsonObject(with: data, options: []) as! [[String:Any]]
    
    for item in JSON {
      let tale = Tale(data: item)
      talesDataSource.append(tale)
    }
    
    backButton = UIButton()
    backButton.setBackgroundImage(UIImage(named: "btn_back.png"), for: .normal)
    backButton.backgroundColor = UIColor.clear
    backButton.addTarget(self, action: #selector(back(_:)), for: .touchUpInside)
    
    titleLabel = UILabel()
    titleLabel.textAlignment = .center
    titleLabel.adjustsFontSizeToFitWidth = true
    titleLabel.textColor = UIColor.white
    titleLabel.font = UIFont(name: "PTSans-Regular", size: 20.0)
    titleLabel.text = "Filter"
    
    ageLabel = UILabel()
    ageLabel.textAlignment = .left
    ageLabel.adjustsFontSizeToFitWidth = true
    ageLabel.textColor = UIColor.white
    ageLabel.font = UIFont(name: "PTSans-Regular", size: 16.0)
    ageLabel.text = "Select Age Range"
    
    zeroToTwoButton = createButton(age: "0-2")
    zeroToTwoButton.addTarget(self, action: #selector(changeColour(_:)), for: .touchUpInside)
    
    twoToFourButton = createButton(age: "2-4")
    twoToFourButton.addTarget(self, action: #selector(changeColour(_:)), for: .touchUpInside)
    
    fourToSixButton = createButton(age: "4-6")
    fourToSixButton.addTarget(self, action: #selector(changeColour(_:)), for: .touchUpInside)
    
    sixToEghtButton = createButton(age: "6-8")
    sixToEghtButton.addTarget(self, action: #selector(changeColour(_:)), for: .touchUpInside)
    
    categotiesLabel = UILabel()
    categotiesLabel.textAlignment = .left
    categotiesLabel.adjustsFontSizeToFitWidth = true
    categotiesLabel.textColor = UIColor.white
    categotiesLabel.font = UIFont(name: "PTSans-Regular", size: 16.0)
    categotiesLabel.text = "Select Categories"
    
    adventureView = customView(title: "Adventure & Action", image: "Filter_Adventures")
    animalView = customView(title: "Animals", image: "Filter_Animals")
    classicView = customView(title: "Classics", image: "Filter_Classics")
    craftView = customView(title: "Craft & Hobby", image: "Filter_Craft")
    fairyView = customView(title: "Fairy Tales", image: "Filter_FairyTales")
    familyView = customView(title: "Family & Friends", image: "Filter_Family")
    fantasyView = customView(title: "Fantasy", image: "Filter_Fantasy")
    funView = customView(title: "Fun", image: "Filter_Fun")
    learningView = customView(title: "Learning", image: "Filter_Learning")
    
    let gestureAdvents = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
    adventureView.addGestureRecognizer(gestureAdvents)
    
    let gestureAnim = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
    animalView.addGestureRecognizer(gestureAnim)
    
    let gestureClass = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
    classicView.addGestureRecognizer(gestureClass)
    
    let gestureCraft = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
    craftView.addGestureRecognizer(gestureCraft)
    
    let gestureFairy = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
    fairyView.addGestureRecognizer(gestureFairy)
    
    let gestureFamily = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
    familyView.addGestureRecognizer(gestureFamily)
    
    let gestureFant = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
    fantasyView.addGestureRecognizer(gestureFant)
    
    let gesturFun = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
    funView.addGestureRecognizer(gesturFun)
    
    let gesturLearn = UITapGestureRecognizer(target: self, action:  #selector(checkAction(sender:)))
    learningView.addGestureRecognizer(gesturLearn)
    
    collectionLabel = UILabel()
    collectionLabel.textAlignment = .left
    collectionLabel.adjustsFontSizeToFitWidth = true
    collectionLabel.textColor = UIColor.white
    collectionLabel.font = UIFont(name: "PTSans-Regular", size: 16.0)
    collectionLabel.text = "Featured Collection"
    
    pickataleButton = createButton(age: "Pickatale")
    pickataleButton.addTarget(self, action: #selector(changeColour(_:)), for: .touchUpInside)
    
    audiobooksButton = createButton(age: "Audiobooks")
    audiobooksButton.addTarget(self, action: #selector(changeColour(_:)), for: .touchUpInside)
    
    truecolorButton = createButton(age: "Truecolor")
    truecolorButton.addTarget(self, action: #selector(changeColour(_:)), for: .touchUpInside)
    
    applyFilterButton = UIButton()
    applyFilterButton.setTitle("Apply Filter", for: .normal)
    applyFilterButton.setTitleColor(UIColor.white, for: .normal)
    applyFilterButton.titleLabel?.textAlignment = .center
    applyFilterButton.layer.cornerRadius = 15.0
    applyFilterButton.backgroundColor = mainButtonColor
    applyFilterButton.addTarget(self, action: #selector(applyFilter(_:)), for: .touchUpInside)
    
    self.view.addSubview(backgroundImage)
    
    self.view.addSubview(backButton)
    self.view.addSubview(titleLabel)
    
    self.view.addSubview(ageLabel)
    self.view.addSubview(sixToEghtButton)
    self.view.addSubview(fourToSixButton)
    self.view.addSubview(twoToFourButton)
    self.view.addSubview(zeroToTwoButton)
    
    self.view.addSubview(categotiesLabel)
    self.view.addSubview(adventureView)
    self.view.addSubview(animalView)
    self.view.addSubview(classicView)
    self.view.addSubview(craftView)
    self.view.addSubview(fairyView)
    self.view.addSubview(familyView)
    self.view.addSubview(fantasyView)
    self.view.addSubview(funView)
    self.view.addSubview(learningView)
    
    self.view.addSubview(collectionLabel)
    self.view.addSubview(pickataleButton)
    self.view.addSubview(audiobooksButton)
    self.view.addSubview(truecolorButton)
    
    self.view.addSubview(applyFilterButton)
  }
  
  func setupConstraints() {
    backgroundImage.snp.makeConstraints{ make in
      make.left.right.top.bottom.equalTo(self.view)
    }
    
    backButton.snp.makeConstraints{ make in
      make.top.equalTo(self.view).offset(20)
      make.left.equalTo(self.view).offset(10)
      make.height.width.equalTo(self.view.frame.width/13)
    }
    
    titleLabel.snp.makeConstraints{ make in
      make.centerY.equalTo(backButton.snp.centerY)
      make.left.equalTo(backButton.snp.right).offset(10)
    }
    
    ageLabel.snp.makeConstraints{ make in
      make.top.equalTo(titleLabel.snp.bottom).offset(30)
      make.left.equalTo(self.view).offset(10)
    }
    
    twoToFourButton.snp.makeConstraints{ make in
      make.top.equalTo(ageLabel.snp.bottom).offset(10)
      make.centerX.equalTo(self.view).offset(-40)
      make.width.height.equalTo(40)
    }
    
    fourToSixButton.snp.makeConstraints{ make in
      make.top.equalTo(ageLabel.snp.bottom).offset(10)
      make.centerX.equalTo(self.view).offset(40)
      make.width.height.equalTo(40)
    }
    
    sixToEghtButton.snp.makeConstraints{ make in
      make.top.equalTo(ageLabel.snp.bottom).offset(10)
      make.left.equalTo(fourToSixButton.snp.right).offset(20)
      make.width.height.equalTo(40)
    }
    
    zeroToTwoButton.snp.makeConstraints{ make in
      make.top.equalTo(ageLabel.snp.bottom).offset(10)
      make.right.equalTo(twoToFourButton.snp.left).offset(-20)
      make.width.height.equalTo(40)
    }
    
    categotiesLabel.snp.makeConstraints{ make in
      make.top.equalTo(zeroToTwoButton.snp.bottom).offset(20)
      make.left.equalTo(self.view).offset(10)
    }
    
    
    animalView.snp.makeConstraints{ make in
      make.top.equalTo(categotiesLabel.snp.bottom).offset(10)
      make.centerX.equalTo(self.view)
      make.width.equalTo(self.view.frame.width/4)
      make.height.equalTo(self.view.frame.height/8)
    }
    
    adventureView.snp.makeConstraints{ make in
      make.top.equalTo(categotiesLabel.snp.bottom).offset(10)
      make.right.equalTo(animalView.snp.left).offset(-20)
      make.width.equalTo(self.view.frame.width/4)
      make.height.equalTo(self.view.frame.height/8)
    }
    
    classicView.snp.makeConstraints{ make in
      make.top.equalTo(categotiesLabel.snp.bottom).offset(10)
      make.left.equalTo(animalView.snp.right).offset(20)
      make.width.equalTo(self.view.frame.width/4)
      make.height.equalTo(self.view.frame.height/8)
    }
    
    fairyView.snp.makeConstraints{ make in
      make.top.equalTo(animalView.snp.bottom).offset(10)
      make.centerX.equalTo(self.view)
      make.width.equalTo(self.view.frame.width/4)
      make.height.equalTo(self.view.frame.height/8)
    }
    
    craftView.snp.makeConstraints{ make in
      make.centerY.equalTo(fairyView.snp.centerY)
      make.right.equalTo(fairyView.snp.left).offset(-20)
      make.width.equalTo(self.view.frame.width/4)
      make.height.equalTo(self.view.frame.height/8)
    }
    
    familyView.snp.makeConstraints{ make in
      make.centerY.equalTo(fairyView.snp.centerY)
      make.left.equalTo(fairyView.snp.right).offset(20)
      make.width.equalTo(self.view.frame.width/4)
      make.height.equalTo(self.view.frame.height/8)
    }
    
    funView.snp.makeConstraints{ make in
      make.top.equalTo(fairyView.snp.bottom).offset(10)
      make.centerX.equalTo(self.view)
      make.width.equalTo(self.view.frame.width/4)
      make.height.equalTo(self.view.frame.height/8)
    }
    
    fantasyView.snp.makeConstraints{ make in
      make.centerY.equalTo(funView.snp.centerY)
      make.right.equalTo(funView.snp.left).offset(-20)
      make.width.equalTo(self.view.frame.width/4)
      make.height.equalTo(self.view.frame.height/8)
    }
    
    learningView.snp.makeConstraints{ make in
      make.centerY.equalTo(funView.snp.centerY)
      make.left.equalTo(funView.snp.right).offset(20)
      make.width.equalTo(self.view.frame.width/4)
      make.height.equalTo(self.view.frame.height/8)
    }
    
    
    collectionLabel.snp.makeConstraints{ make in
      make.top.equalTo(fantasyView.snp.bottom).offset(20)
      make.left.equalTo(self.view).offset(10)
    }
    audiobooksButton.snp.makeConstraints{ make in
      make.top.equalTo(collectionLabel.snp.bottom).offset(20)
      make.centerX.equalTo(self.view)
      make.width.equalTo(self.view.frame.width/4)
    }
    pickataleButton.snp.makeConstraints{ make in
      make.centerY.equalTo(audiobooksButton.snp.centerY)
      make.right.equalTo(audiobooksButton.snp.left).offset(-20)
      make.width.equalTo(self.view.frame.width/4)
    }
    truecolorButton.snp.makeConstraints{ make in
      make.centerY.equalTo(audiobooksButton.snp.centerY)
      make.left.equalTo(audiobooksButton.snp.right).offset(20)
      make.width.equalTo(self.view.frame.width/4)
    }
    
    applyFilterButton.snp.makeConstraints{ make in
      make.left.equalTo(self.view).offset(20)
      make.right.equalTo(self.view).offset(-20)
      make.bottom.equalTo(self.view).offset(-20)
      make.height.equalTo(55)
    }
  }
  
  func customView(title: String, image: String) -> UIView {
    let view = UIView()
    
    let imageV = UIImageView()
    imageV.contentMode = .scaleAspectFit
    imageV.image = UIImage(named: image)
    
    let label = UILabel()
    label.textAlignment = .center
    label.adjustsFontSizeToFitWidth = true
    label.textColor = UIColor.white
    label.font = UIFont(name: "PTSans-Regular", size: 11.0)
    label.text = title
    
    view.addSubview(imageV)
    view.addSubview(label)
    
    label.snp.makeConstraints{ make in
      make.centerX.equalTo(view.snp.centerX)
      make.bottom.equalTo(view.snp.bottom)
    }
    
    imageV.snp.makeConstraints{ make in
      make.bottom.equalTo(label.snp.top).offset(-10)
      make.left.top.equalTo(view).offset(10)
      make.right.equalTo(view).offset(-10)
    }
    
    return view
  }
  
  func createButton(age: String) -> UIButton {
    let button = UIButton()
    button.backgroundColor = UIColor.clear
    button.setTitle(age, for: .normal)
    button.setTitleColor(UIColor.white, for: .normal)
    button.titleLabel?.textAlignment = .center
    button.titleLabel?.font = UIFont(name: "PTSans-Bold", size: 16.0)
    return button
  }
  
  func checkAction(sender : UITapGestureRecognizer) {
    if sender.view?.backgroundColor == UIColor(colorLiteralRed: 255 / 255, green: 179 / 255, blue: 13 / 255, alpha: 1.0) {
      sender.view?.backgroundColor = .clear
    } else {
      sender.view?.backgroundColor = UIColor(colorLiteralRed: 255 / 255, green: 179 / 255, blue: 13 / 255, alpha: 1.0)
    }
  }
  
  func changeColour(_ sender: UIButton) {
    if sender.backgroundColor == UIColor(colorLiteralRed: 255 / 255, green: 179 / 255, blue: 13 / 255, alpha: 1.0) {
      sender.backgroundColor = .clear
    } else {
      sender.backgroundColor = UIColor(colorLiteralRed: 255 / 255, green: 179 / 255, blue: 13 / 255, alpha: 1.0)
    }
  }
  
  func applyFilter(_ sender: UIButton){
    let presentVC = PresentTalesViewController()
    presentVC.mainTitle = "Filtered Books"
    presentVC.dataSource = talesDataSource
    
    self.navigationController?.pushViewController(presentVC, animated: false)
  }
  
  func back(_ sender: UIButton){
    _ = self.navigationController?.popViewController(animated: true)
  }
  
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}


