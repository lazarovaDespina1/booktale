//
//  Tale.swift
//  Book
//
//  Created by Jovan on 13.3.18.
//  Copyright © 2018 ECHO. All rights reserved.
//

import Foundation

class Tale {
  var id: Int!
  var name: String!
  var imageName: String!
  var isFav: Bool!
  var age: Int!
  
  init(data: [String: Any]) {
    self.id = data["id"] as! Int!
    self.name = data["name"] as! String!
    self.imageName = data["image"] as! String!
    self.isFav = data["isFavourite"] as! Bool!
    self.age = data["age"] as! Int!
  }
}
